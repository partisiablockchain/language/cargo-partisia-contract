use std::env;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fs::{create_dir_all, File};
use std::io::{BufReader, Write};
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::time::Duration;

use cargo_metadata::{Metadata, Package};
use log::{debug, error, trace};
use netrc::{Machine, Netrc};
use reqwest::blocking::Response;
use reqwest::header::HeaderValue;
use reqwest::StatusCode;
use serde_json::Value;

use crate::build_command::build_package_error::BuildPackageError;
use crate::constants::{ABI_CLI, PARTISIA_CLI};
use crate::get_compiler::file_url_parser::Url::GenericUrl;
use crate::get_compiler::file_url_parser::{ParseUrlError, Url};
use crate::jar_command::{get_fetch_metadata_or_default, pbc_folder};
use crate::package_handler::utils::get_target_folder_metadata;

pub(crate) mod file_url_parser;

static_toml::static_toml! {
    static CARGO_TOML = include_toml!("Cargo.toml");
}

/// Enum for the metadata expected in the contract toml to fetch a jar.
#[derive(Clone)]
pub(crate) enum FetchMetadata {
    /// Variant for getting the jar using Http, should use the host of the url to get netrc
    /// authentication.
    ///
    /// ### Fields:
    ///
    /// * `url`: [`String`], the url to call.
    Http { url: String },
}

impl FetchMetadata {
    pub(crate) fn default(artifact: &str) -> FetchMetadata {
        match artifact {
            PARTISIA_CLI => FetchMetadata::Http {
                url: CARGO_TOML.package.metadata.partisia_cli.url.to_string(),
            },
            ABI_CLI => FetchMetadata::Http {
                url: CARGO_TOML.package.metadata.abi_cli.url.to_string(),
            },
            s => {
                panic!("Tried to get default fetchdata for {s}, which does not have a default.");
            }
        }
    }
}

impl FromStr for FetchMetadata {
    type Err = ParseUrlError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let url = Url::from_str(value)?;
        match url {
            GenericUrl { url } => Ok(FetchMetadata::Http { url }),
        }
    }
}

impl Display for FetchMetadata {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FetchMetadata::Http { url } => {
                write!(f, "Http, url: {url}")
            }
        }
    }
}

/// Retrieve the zk-compiler. Looks at the [package.metadata.zkcompiler] located in the contract toml
/// to decide how the compiler should be fetched.
///
/// ### Parameters:
///
/// * `check_existing`: [`bool`], if true will not fetch a new compiler if there already exist one
///     with the given version in the expected location.
///
/// * `package`: &[`Package`], the package to retrieve the zk compiler for.
///
/// * `metadata`: &[`Metadata`], cargo metadata for the project.
///
/// ### Returns:
///
/// A [`PathBuf`] containing the path to the zk-compiler.jar
pub(crate) fn retrieve_zk_compiler(
    check_existing: bool,
    package: &Package,
    metadata: &Metadata,
) -> Result<PathBuf, Box<dyn Error>> {
    if let Some(zk_compiler_metadata) =
        get_fetch_metadata(&metadata.workspace_metadata, package, "zkcompiler")
    {
        let zk_compiler_path = get_target_folder_metadata(metadata);
        update_save_path_and_fetch(
            zk_compiler_metadata,
            &zk_compiler_path,
            "zk-compiler",
            check_existing,
        )
    } else {
        error!("Failed to get metadata for retrieving the zk-compiler.");
        Err(BuildPackageError.into())
    }
}

pub(crate) fn get_file_name(fetch_metadata: FetchMetadata, default_artifact: &str) -> String {
    let url = match fetch_metadata {
        FetchMetadata::Http { url, .. } => url,
    };
    PathBuf::from(url)
        .file_name()
        .and_then(|os_str| os_str.to_str())
        .unwrap_or(default_artifact)
        .to_string()
}

/// Retrieve the jar for the given `artifact`. If no url is given a user configuration file in
/// ~/.cargo-partisia-contract.toml or default values will be used.
///
/// ### Parameters:
///
/// * `url`: &[`Option<String>`], If given uses this url for fetching the jar.
///
/// ### Returns:
///
/// A [`PathBuf`] containing the path to the jar.
pub(crate) fn retrieve_jar(
    url: &Option<String>,
    artifact: &str,
) -> Result<PathBuf, Box<dyn Error>> {
    let pbc_folder = pbc_folder();
    let fetch_metadata = if let Some(url) = url {
        FetchMetadata::from_str(url)?
    } else {
        get_fetch_metadata_or_default(artifact)
    };
    update_save_path_and_fetch(fetch_metadata, &pbc_folder, artifact, true)
}

fn update_save_path_and_fetch(
    fetch_metadata: FetchMetadata,
    pbc_folder: &Path,
    default_artifact: &str,
    check_existing: bool,
) -> Result<PathBuf, Box<dyn Error>> {
    let artifact_file_name = get_file_name(fetch_metadata.clone(), default_artifact);
    debug!(
        "Fetching {}: {}, using {}",
        default_artifact, artifact_file_name, fetch_metadata
    );
    if !pbc_folder.exists() {
        create_dir_all(pbc_folder)?;
    }
    let mut save_path = PathBuf::from(pbc_folder);
    save_path.push(artifact_file_name.clone());

    match fetch_metadata {
        FetchMetadata::Http { url } => {
            if check_existing && save_path.exists() {
                debug!("Using existing {}", artifact_file_name);
                return Ok(save_path);
            }
            get_file_http(&url, &save_path)?;
            Ok(save_path)
        }
    }
}

/// Retrieve a file using http and save it in `save_path`.
///
/// ### Parameters:
///
/// * `url`: [`&str`], the url to make the http request to.
///
/// * `save_path`: &[`Path`], the location for storing the retrieved file jar.
pub(crate) fn get_file_http(url: &str, save_path: &Path) -> Result<(), Box<dyn Error>> {
    let client = reqwest::blocking::Client::new();
    let mut request_builder = client.get(url);
    request_builder = request_builder.timeout(Duration::from_secs(300));
    let netrc_machine = get_netrc_machine(url);
    if let Some(machine) = &netrc_machine {
        request_builder = request_builder.basic_auth(&machine.login, machine.password.as_ref());
    }

    let response = request_builder.send().inspect_err(|e| {
        error!("Failed to get file from: {url}, got error: {e}");
    })?;
    if response.status().is_success() {
        check_jar(&response)?;
        let bytes = response.bytes()?;
        create_dir_all(save_path.parent().unwrap())?;
        let mut zk_compiler_file = File::create(save_path).inspect_err(|e| {
            error!(
                "Failed to create file at {}, got error {e}",
                save_path.to_str().unwrap_or("")
            );
        })?;
        zk_compiler_file.write_all(bytes.as_ref())?;
    } else {
        error!(
            "Failed to get {url} using http - Error {}",
            response.status()
        );
        if response.status() == StatusCode::from_u16(401).unwrap() && netrc_machine.is_none() {
            let parsed_url = url::Url::parse(url).unwrap();
            let host = parsed_url.host_str().unwrap_or(url);
            error!("netrc file is missing authentication for {host}");
        }
        return Err(BuildPackageError.into());
    }
    Ok(())
}

fn check_jar(response: &Response) -> Result<(), Box<dyn Error>> {
    if let Some(content_type) = response.headers().get("content-type") {
        for header in valid_header_values() {
            if header.eq(content_type) {
                return Ok(());
            }
        }
    }
    error!("Downloaded file was not a JAR");
    Err(BuildPackageError.into())
}

fn valid_header_values() -> Vec<HeaderValue> {
    let jar_content_type = HeaderValue::from_str("application/java-archive").unwrap();
    let octet_stream = HeaderValue::from_str("application/octet-stream").unwrap();
    vec![jar_content_type, octet_stream]
}

/// Gets the specific machine from netrc file which should be used for querying the given url.
///
/// ### Parameters:
///
/// * `url`: [`&str`], the url used for finding the specific machine in _netrc.
///
/// ### Returns:
///
/// An [`Option<Machine>`] from the netrc file.
fn get_netrc_machine(url: &str) -> Option<Machine> {
    let netrc_file = open_netrc()?;
    let netrc_res = Netrc::parse(BufReader::new(netrc_file));
    if netrc_res.is_err() {
        error!("{:?}", netrc_res.unwrap_err());
        error!("Failed to parse netrc file");
        return None;
    }
    let netrc = netrc_res.unwrap();

    let url_res = url::Url::parse(url);
    if url_res.is_err() {
        error!("{:?}", url_res.unwrap_err());
        error!("Failed to parse url while getting netrc machine");
        return None;
    }
    let url = url_res.unwrap();
    let host = url.host().unwrap_or(url::Host::Domain(""));
    for net_host in netrc.hosts {
        let v = url::Host::parse(net_host.0.as_str());
        if v.is_ok() && v.unwrap() == host {
            return Some(net_host.1);
        }
    }
    netrc.default
}

/// Extracts the metadata for `metadata_index` from the contracts toml. The expected metadata should be in
/// [package.metadata.metadata_index] or [workspace.metadata.metadata_index] and include an `url` item.
///
/// ### Parameters:
/// * `workspace_metadata`: &[`Value`], the workspace metadata.
///
/// * `package`: &[`Package`], the package to get the package metadata from.
///
/// * `metadata_index`: [`&str`], the metadata_index to get the metadata from.
///
/// ### Returns:
///
/// An [`Option<FetchMetadata>`] representing the defined metadata. `None` if the metadata wasn't defined.
pub(crate) fn get_fetch_metadata(
    workspace_metadata: &Value,
    package: &Package,
    metadata_index: &str,
) -> Option<FetchMetadata> {
    let extract_url = |metadata: &Value| {
        let url = extract_string_val(metadata, "url").or_else(|| {
            error!(
                "Failed to get {} metadata, expected url to be a String",
                metadata_index
            );
            None
        })?;
        match FetchMetadata::from_str(&url) {
            Ok(fetch_metadata) => Some(fetch_metadata),
            Err(err) => {
                error!("{err}");
                None
            }
        }
    };

    if let Some(metadata) = package.metadata.get(metadata_index) {
        extract_url(metadata)
    } else if let Some(metadata) = workspace_metadata.get(metadata_index) {
        extract_url(metadata)
    } else {
        None
    }
}

fn extract_string_val(metadata: &Value, val: &str) -> Option<String> {
    let option_val = metadata.get(val);
    option_val.and_then(|value| serde_json::from_value(value.clone()).ok())
}

/// Method for getting the paths with file prefix to the zk computation.
/// The computation is expected in the contracts toml [package.metadata.zk] with id zk-compute-path.
/// It can either be an array or a string value.
///
/// ### Params:
/// * `package`: &[`Package`], the package to get the zk compute path from.
///
/// ### Returns:
/// An [`Option<Vec<String>>`] representing the paths to the zk computation files.
/// `None` if not defined, or if the array was empty.
pub(crate) fn get_zk_compute_paths(
    package: &Package,
) -> Result<Option<Vec<PathBuf>>, Box<dyn Error>> {
    let Some(zk_metadata) = package.metadata.get("zk") else {
        return Ok(None);
    };
    let Some(compute_file_value) = zk_metadata.get("zk-compute-path") else {
        return Ok(None);
    };
    let zk_compute_files = if compute_file_value.is_array() {
        compute_file_value.as_array().unwrap().clone()
    } else if compute_file_value.is_string() {
        let compute_file = compute_file_value.clone();
        vec![compute_file]
    } else {
        Err("Expected zk-compute-path to be a String or an Array of Strings")?
    };
    let paths: Option<Vec<PathBuf>> = zk_compute_files
        .iter()
        .map(|value| {
            let compute_file: String =
                serde_json::from_value(value.clone()).unwrap_or_else(|_| {
                    error!("Expected zk-compute-path to be a String");
                    None
                })?;
            let mut path = PathBuf::from(package.manifest_path.clone());
            path.pop();
            path.push(compute_file);
            Some(path)
        })
        .collect();
    if let Some(vec_paths) = &paths {
        if vec_paths.is_empty() {
            Err("zk-compute-path is not allowed to be an empty array")?
        }
    }
    Ok(paths)
}

/// Finds and opens the netrc file. If no netrc file exists in either the home directory or
/// in the $NETRC directory returns none.
///
/// ### Returns:
///
/// The netrc file as [`Option<File>`] or `None` if no such file is found.
fn open_netrc() -> Option<File> {
    let path = match env::var_os("NETRC") {
        Some(path) => Some(PathBuf::from(path)),
        None => {
            let home_dir = dirs::home_dir()?;
            let possible_files = [".netrc", "_netrc"];
            let mut possible_paths = possible_files
                .iter()
                .map(|possible_file| home_dir.join(possible_file));

            possible_paths.find(|path| path.exists())
        }
    }?;
    if let Ok(file) = File::open(&path) {
        trace!("Using NETRC file located at: {}", path.display());
        Some(file)
    } else {
        None
    }
}
