use std::error::Error;
use std::string::ToString;

use crate::constants::PARTISIA_CLI;
use crate::get_compiler::retrieve_jar;
use crate::jar_command::{get_version_used, CallableJar};

pub const TRANSACTION: &str = "transaction";
pub const ACCOUNT: &str = "account";
pub const WALLET: &str = "wallet";
pub const CONTRACT: &str = "contract";
pub const CONFIG: &str = "config";
pub const BLOCK: &str = "block";
pub const ABI: &str = "abi";

pub(crate) struct PartisiaCli {
    url: Option<String>,
    help: bool,
}

impl PartisiaCli {
    pub(crate) fn new(url: Option<String>, help: bool) -> PartisiaCli {
        PartisiaCli { url, help }
    }

    /// Fetch and run the partisia-cli.jar with the given trailing var arguments.
    /// If given a url, uses this to fetch the jar and then updates the config.
    ///
    /// ### Parameters:
    ///
    /// * `url`: &[`Option<String>`], If given uses this url for fetching the partisia-cli.jar.
    ///
    /// * `additional_args`: &[`Vec<String>`], The trailing args that are passed to the partisia-cli
    ///     child process.
    ///
    ///
    /// ### Returns:
    ///
    /// An empty result or a heap allocated Error.
    fn partisia_cli_command(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let cli = CallableJar::from(retrieve_jar(&self.url, PARTISIA_CLI)?);
        cli.save_jar_fetch_config(PARTISIA_CLI)?;
        cli.call_jar(&additional_args, None, None)?;
        Ok(())
    }

    pub(crate) fn transaction(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let transaction_command = self.build_cli_command(additional_args, TRANSACTION);
        self.partisia_cli_command(transaction_command)
    }

    pub(crate) fn contract(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let contract_command = self.build_cli_command(additional_args, CONTRACT);
        self.partisia_cli_command(contract_command)
    }

    pub(crate) fn account(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let account_command = self.build_cli_command(additional_args, ACCOUNT);
        self.partisia_cli_command(account_command)
    }

    pub(crate) fn wallet(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let wallet_command = self.build_cli_command(additional_args, WALLET);
        self.partisia_cli_command(wallet_command)
    }

    pub(crate) fn config(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let config_command = self.build_cli_command(additional_args, CONFIG);
        self.partisia_cli_command(config_command)
    }

    pub(crate) fn block(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let block_command = self.build_cli_command(additional_args, BLOCK);
        self.partisia_cli_command(block_command)
    }

    pub(crate) fn abi(&self, additional_args: Vec<String>) -> Result<(), Box<dyn Error>> {
        let abi_command = self.build_cli_command(additional_args, ABI);
        self.partisia_cli_command(abi_command)
    }

    pub(crate) fn print_version(&self) -> Result<(), Box<dyn Error>> {
        println!(
            "Partisia-Cli version: {}",
            get_version_used(&self.url, PARTISIA_CLI)
        );
        Ok(())
    }

    fn build_cli_command(&self, mut additional_args: Vec<String>, command: &str) -> Vec<String> {
        additional_args.insert(0, command.to_string());
        if self.help {
            additional_args.push("--help".to_string());
        }
        additional_args
    }
}
