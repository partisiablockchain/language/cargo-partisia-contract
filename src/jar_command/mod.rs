use java_locator::locate_java_home;
use std::cmp::Ordering;
use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::path::{Path, PathBuf};
use std::process::{Command, Output, Stdio};
use std::str::FromStr;
use std::{env, fmt, fs};

use crate::build_command::build_package_error::BuildPackageError;
use log::{debug, error};
use regex::Regex;
use serde_json::from_str;
use toml_edit::{DocumentMut, InlineTable, Item};
use url::Url;

use crate::constants::{ABI_CLI, PARTISIA_CLI};
use crate::constants::{CONFIG_TOML_FILE_NAME, DEFAULT_PBC_FOLDER_NAME, ENV_VAR_PBC_FOLDER};
use crate::get_compiler::{get_file_name, FetchMetadata};

pub(crate) struct JarCommandError {
    artifact: Option<String>,
}

impl JarCommandError {
    pub(crate) fn from(artifact: Option<String>) -> Self {
        Self { artifact }
    }

    pub(crate) fn from_str(artifact: &str) -> Self {
        Self {
            artifact: Some(artifact.to_string()),
        }
    }
}

impl Display for JarCommandError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        if let Some(artifact) = &self.artifact {
            write!(
                f,
                "{artifact} jar command failed, see console output for more details.",
            )
        } else {
            write!(
                f,
                "Jar command failed, see console output for more details.",
            )
        }
    }
}

impl Debug for JarCommandError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "AbiCliError")
    }
}

impl Error for JarCommandError {}

/// Wrapper for a path to a callable jar
pub(crate) struct CallableJar {
    jar_path: PathBuf,
}

impl CallableJar {
    /// Call the jar with the given arguments.
    ///
    /// ### Params
    /// `arguments`: &[`Vec<String>`], list of arguments to the child process.
    ///
    /// `stdout`: [`Option<T>`] where `T: Into<Stdio>`, optional stdout override for the child process.
    ///
    /// ### Returns
    /// The output of the child process.
    pub(crate) fn call_jar(
        &self,
        arguments: &Vec<String>,
        stdout: Option<Stdio>,
        overwrite_err_msg: Option<String>,
    ) -> Result<Output, Box<dyn Error>> {
        let mut process = java_jar_command()?;
        process.arg(&self.jar_path);
        process.args(arguments);
        if let Some(stdout) = stdout {
            process.stdout(stdout);
        }
        if overwrite_err_msg.is_some() {
            process.stderr(Stdio::piped());
        }

        let output = process.output()?;
        if let Some(0) = output.status.code() {
            Ok(output)
        } else {
            if let Some(err_msg) = overwrite_err_msg {
                eprintln!("{}", err_msg);
            }
            Err(JarCommandError::from(None).into())
        }
    }

    /// Save a new config in the pbc folder containing the file url to `abi_client_path`.
    pub(crate) fn save_jar_fetch_config(&self, artifact: &str) -> Result<(), Box<dyn Error>> {
        let config_path = pbc_folder().join(CONFIG_TOML_FILE_NAME);
        debug!(
            "Saving new jar fetch config for {artifact} at {}",
            config_path.display()
        );
        let path_string =
            self.jar_path.clone().into_os_string().into_string().expect(
                "Failed to convert path to string when saving the new abi-cli fetch config",
            );
        let url = Url::from_file_path(&path_string).map_err(|_| {
            error!("Failed to convert {path_string} to a file url");
            JarCommandError::from_str(artifact)
        })?;
        update_config_file(&config_path, artifact, url)
    }
}

/// Locates JAVA_HOME.
///
/// ### Returns:
///
/// A command to launch the Java executable, or a heap allocated Error.
pub(crate) fn java_jar_command() -> Result<Command, Box<dyn Error>> {
    let java = locate_java_home()?;
    debug!("Found JAVA_HOME: {}", java);
    let mut java_exe = PathBuf::from(&java);
    java_exe.push("bin");
    java_exe.push("java");

    if !check_java_executable(&java_exe) {
        error!("Java binary not found!");
        error!("Expected executable bin/java to exist in JAVA_HOME: {java}");
        return Err(BuildPackageError.into());
    }

    let mut process = Command::new(java_exe.to_str().unwrap());
    process
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .arg("-jar");

    Ok(process)
}

fn check_java_executable(java_exe: &Path) -> bool {
    let mut process = Command::new(java_exe.to_str().unwrap());

    process
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .arg("--help")
        .output()
        .is_ok()
}

fn update_config_file(
    config_path: &Path,
    artifact: &str,
    new_url: Url,
) -> Result<(), Box<dyn Error>> {
    let config = fs::read_to_string(config_path).unwrap_or("".to_string());
    let new_url_item = Item::from_str(format!("\'{new_url}\'").as_str())?;
    let mut doc = config.parse::<DocumentMut>()?;
    doc["package"]["metadata"][artifact]["url"] = new_url_item;
    doc["package"]["metadata"][artifact]
        .as_inline_table_mut()
        .map(InlineTable::fmt);
    fs::write(config_path, doc.to_string().as_bytes())?;

    Ok(())
}

impl From<PathBuf> for CallableJar {
    fn from(jar_path: PathBuf) -> Self {
        CallableJar { jar_path }
    }
}

/// Get the path to the pbc folder. By default located in ~/[`DEFAULT_PBC_FOLDER_NAME`] but can be overriden
/// using env variable [`ENV_VAR_PBC_FOLDER`].
///
/// ### Returns:
///
/// The absolute path to the pbc folder.
pub(crate) fn pbc_folder() -> PathBuf {
    if let Ok(pbc_folder) = env::var(ENV_VAR_PBC_FOLDER) {
        PathBuf::from(pbc_folder)
    } else {
        home::home_dir()
            .unwrap_or_else(|| panic!("Failed to get path to ~/{DEFAULT_PBC_FOLDER_NAME}"))
            .join(DEFAULT_PBC_FOLDER_NAME)
    }
}

/// Get the default method for fetching the `artifact` jar. Uses config value if present or
/// hardcoded default values.
///
/// ### Returns:
///
/// The default fetch information.
pub(crate) fn get_fetch_metadata_or_default(artifact: &str) -> FetchMetadata {
    let config_path = pbc_folder().join(CONFIG_TOML_FILE_NAME);
    let config_content = fs::read_to_string(config_path).ok();
    if let Some(config) = config_content {
        let doc = config
            .parse::<DocumentMut>()
            .expect("Invalid config file,  must be on a valid toml format");
        let values = doc
            .get("package")
            .and_then(|v| v.get("metadata"))
            .and_then(|v| v.get(artifact))
            .and_then(|v| v.as_inline_table());
        if let Some(values) = values {
            let url = values.get("url");
            if let Some(found_url) = url {
                let config_url = found_url
                .as_str()
                .unwrap_or_else(|| panic!("Expected url in ~/{DEFAULT_PBC_FOLDER_NAME}/{CONFIG_TOML_FILE_NAME} to be a String"))
                .to_string();
                if config_version_is_newer_than_default_version(config_url.clone()) {
                    return FetchMetadata::from_str(&config_url).unwrap();
                }
            }
        }
    }
    FetchMetadata::default(artifact)
}

pub(crate) fn get_version_used(url: &Option<String>, artifact: &str) -> String {
    if let Some(specific_url) = url {
        get_version_from_url(specific_url.clone())
    } else {
        let metadata = get_fetch_metadata_or_default(artifact);
        get_version_from_url(get_file_name(metadata, artifact))
    }
}

fn config_version_is_newer_than_default_version(url: String) -> bool {
    let config_version = get_version_from_url(url.clone());
    if config_version.eq("NO_VERSION") {
        return true;
    }
    url.contains("partisia-cli")
        && config_version_is_higher(
            config_version.clone(),
            get_version_from_url(get_jar_url(PARTISIA_CLI)),
        )
        || (url.contains("abi-client")
            && config_version_is_higher(
                config_version.clone(),
                get_version_from_url(get_jar_url(ABI_CLI)),
            ))
}

fn get_jar_url(jar_identifier: &str) -> String {
    static_toml::static_toml! {
        static CARGO_TOML = include_toml!("Cargo.toml");
    }
    match jar_identifier {
        PARTISIA_CLI => CARGO_TOML.package.metadata.partisia_cli.url.to_string(),
        ABI_CLI => CARGO_TOML.package.metadata.abi_cli.url.to_string(),
        s => {
            panic!("Tried to get default fetchdata for {s}, which does not have a default.");
        }
    }
}

fn get_version_from_url(url: String) -> String {
    let regex = Regex::new(r".*-(?<version>\d+\.\d+\.\d+(-beta-par-(\d|\w)+-(\d|\w)+-(\d|\w)+)?)-jar-with-dependencies\.jar").unwrap();
    let Some(caps) = regex.captures(url.as_str()) else {
        return "NO_VERSION".to_string();
    };
    caps["version"].to_string()
}

fn config_version_is_higher(config_version: String, default_version: String) -> bool {
    let config_version_split: Vec<&str> = config_version.split('.').collect();
    let default_version_split: Vec<&str> = default_version.split('.').collect();
    for i in 0..3 {
        let config_as_int: i64 =
            from_str::<i64>(config_version_split[i]).expect("Expect an integer");
        let default_as_int: i64 =
            from_str::<i64>(default_version_split[i]).expect("Expect an integer");

        match config_as_int.cmp(&default_as_int) {
            Ordering::Greater => return true,
            Ordering::Less => return false,
            Ordering::Equal => continue,
        }
    }
    true
}
