use crate::build_command::build_package::check_wasm32_unknown_unknown;

#[test]
fn target_is_present() {
    let result = check_wasm32_unknown_unknown(
        "wasm32-unknown-unknown\n\
            x86_64-pc-windows-msvc"
            .to_string(),
    );
    assert!(result.is_ok());
}

#[test]
fn target_is_missing() {
    let result = check_wasm32_unknown_unknown("x86_64-pc-windows-msvc".to_string());
    assert!(result.is_err());
    assert_eq!(
        result.unwrap_err().to_string(),
        "Build failed, see console output for more details."
    );
}
