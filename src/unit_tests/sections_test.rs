use crate::build_command::section_identifiers::{
    ABI_IDENTIFIER_BYTE, WASM_IDENTIFIER_BYTE, ZK_IDENTIFIER_BYTE,
};
use crate::build_command::sections::*;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

/// Number of bytes in a section identifier.
pub const SECTION_IDENTIFIER_BYTE_LENGTH: usize = 1;
/// Number of bytes used to convey the byte length of a section.
pub const SECTION_LENGTH_BYTE_LENGTH: usize = 4;
/// number of bytes in the .pbc file header.
///
/// Test suite for sections.rs
/// The tests ensure that well-formed, sectioned bytes are produced.
/// A section consists of a header byte, the length of the section's content as a u32
/// and then the section content.
///
/// In the .pbc format, the sections of the file are preceded by the 4 PBC header bytes.
/// Adding an ABI section, a WASM section and a ZKBC section yields the bytes of
/// 3 well-formed sections, each with a correct header byte, a u32 indicating
/// the length of the content and then the section content.
#[test]
fn add_abi_and_wasm_and_zkbc_to_sections() {
    // read files
    let abi_bytes = read_resource_file("zk_voting_simple.abi");
    let wasm_bytes = read_resource_file("zk_voting_simple.wasm");
    let zkbc_bytes = read_resource_file("zk_voting_simple.zkbc");

    // create constants
    let abi_bytes_len = abi_bytes.len();
    let abi_length_start_idx = SECTION_IDENTIFIER_BYTE_LENGTH;

    let wasm_bytes_len = wasm_bytes.len();
    let wasm_sect_start_idx =
        SECTION_LENGTH_BYTE_LENGTH + SECTION_IDENTIFIER_BYTE_LENGTH + abi_bytes_len;

    let zkbc_bytes_len = zkbc_bytes.len();
    let zkbc_sect_start_idx = 2 * (SECTION_LENGTH_BYTE_LENGTH + SECTION_IDENTIFIER_BYTE_LENGTH)
        + abi_bytes_len
        + wasm_bytes_len;

    // add sections
    let mut sections = Sections::new();
    let abi_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.abi");
    sections.add_section(ABI_IDENTIFIER_BYTE, &abi_file);
    let wasm_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.wasm");
    sections.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);
    let zkbc_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.zkbc");
    sections.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);

    // read output
    let output_bytes = sections.get_pbc_bytes();

    // make assertions
    assert_eq!(
        sections.get_pbc_bytes().len(),
        abi_bytes_len
            + wasm_bytes_len
            + zkbc_bytes_len
            + 3 * SECTION_IDENTIFIER_BYTE_LENGTH
            + 3 * SECTION_LENGTH_BYTE_LENGTH
    );
    assert_eq!(output_bytes[0], ABI_IDENTIFIER_BYTE);
    assert_eq!(
        output_bytes[abi_length_start_idx..abi_length_start_idx + SECTION_LENGTH_BYTE_LENGTH],
        u32::to_be_bytes(abi_bytes_len as u32)
    );

    assert_eq!(output_bytes[wasm_sect_start_idx], WASM_IDENTIFIER_BYTE);
    assert_eq!(
        output_bytes[(wasm_sect_start_idx + SECTION_IDENTIFIER_BYTE_LENGTH)
            ..(wasm_sect_start_idx + SECTION_IDENTIFIER_BYTE_LENGTH + SECTION_LENGTH_BYTE_LENGTH)],
        u32::to_be_bytes(wasm_bytes_len as u32)
    );

    assert_eq!(output_bytes[zkbc_sect_start_idx], ZK_IDENTIFIER_BYTE);
    assert_eq!(
        output_bytes[(zkbc_sect_start_idx + SECTION_IDENTIFIER_BYTE_LENGTH)
            ..(zkbc_sect_start_idx + SECTION_IDENTIFIER_BYTE_LENGTH + SECTION_LENGTH_BYTE_LENGTH)],
        u32::to_be_bytes(zkbc_bytes_len as u32)
    );
}

/// Adding a WASM section before an ABI section still yields ordered and well-formed,
/// sectioned bytes.
#[test]
fn add_wasm_before_abi_to_sections() {
    // create file references
    let abi_file = PathBuf::from("src/unit_tests/resources/voting.abi");
    let wasm_file = PathBuf::from("src/unit_tests/resources/voting.wasm");

    // add sections
    let mut sections_regular_order = Sections::new();
    sections_regular_order.add_section(ABI_IDENTIFIER_BYTE, &abi_file);
    sections_regular_order.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);

    let mut sections_reverse_order = Sections::new();
    sections_reverse_order.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);
    sections_reverse_order.add_section(ABI_IDENTIFIER_BYTE, &abi_file);

    // read output
    let regular_order_output_bytes = sections_regular_order.get_pbc_bytes();
    let reverse_order_output_bytes = sections_reverse_order.get_pbc_bytes();

    // make assertions
    assert_eq!(regular_order_output_bytes, reverse_order_output_bytes);
}

/// Adding a ZKBC section before WASM and ABI sections still yields ordered
/// and well-formed, sectioned bytes.
#[test]
fn add_zkbc_before_abi_and_wasm_to_sections() {
    // create file references
    let abi_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.abi");
    let wasm_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.wasm");
    let zkbc_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.zkbc");

    // add sections
    let mut sections_regular_order = Sections::new();
    sections_regular_order.add_section(ABI_IDENTIFIER_BYTE, &abi_file);
    sections_regular_order.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);
    sections_regular_order.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);

    let mut sections_zkbc_first = Sections::new();
    sections_zkbc_first.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);
    sections_zkbc_first.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);
    sections_zkbc_first.add_section(ABI_IDENTIFIER_BYTE, &abi_file);

    // read output
    let regular_order_output_bytes = sections_regular_order.get_pbc_bytes();
    let zkbc_first_output_bytes = sections_zkbc_first.get_pbc_bytes();

    // make assertions
    assert_eq!(regular_order_output_bytes, zkbc_first_output_bytes);
}

/// Adding an ABI section, a WASM section and a ZKBC section and then
/// building ZKWA yields a well-formed WASM section followed by a well-formed
/// ZKBC section.
#[test]
fn add_abi_and_wasm_and_zkbc_and_get_zkwa() {
    // add sections
    let mut sections = Sections::new();
    let abi_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.abi");
    sections.add_section(ABI_IDENTIFIER_BYTE, &abi_file);
    let wasm_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.wasm");
    sections.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);
    let zkbc_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.zkbc");
    sections.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);

    // read output
    let output_bytes = sections.get_zkwa_bytes();
    // read reference
    let zkwa_bytes = read_resource_file("zk_voting_simple.zkwa");
    // assert
    assert_eq!(output_bytes, zkwa_bytes)
}

/// Attempting to construct the bytes of a .pbc file before an ABI section has been added
/// results in an error.
#[test]
#[should_panic(expected = "Cannot build .pbc file: No ABI section provided")]
fn get_pbc_no_abi() {
    // add sections
    let mut sections = Sections::new();
    let wasm_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.wasm");
    sections.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);
    let zkbc_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.zkbc");
    sections.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);

    // read output
    sections.get_pbc_bytes();
}

/// Attempting to construct the bytes of a .pbc file before a WASM section has been added
/// results in an error.
#[test]
#[should_panic(expected = "Cannot build .pbc file: No WASM section provided")]
fn get_pbc_no_wasm() {
    // add sections
    let mut sections = Sections::new();
    let abi_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.abi");
    sections.add_section(ABI_IDENTIFIER_BYTE, &abi_file);
    let zkbc_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.zkbc");
    sections.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);

    // read output
    sections.get_pbc_bytes();
}

/// Attempting to construct the bytes of a .zkwa file before a WASM section has been added
/// results in an error.
#[test]
#[should_panic(expected = "Cannot build .zkwa file: No WASM section provided")]
fn get_zkwa_no_wasm() {
    // add sections
    let mut sections = Sections::new();
    let zkbc_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.zkbc");
    sections.add_section(ZK_IDENTIFIER_BYTE, &zkbc_file);

    // read output
    sections.get_zkwa_bytes();
}

/// Attempting to construct the bytes of a .zkwa file before a ZKBC section has been added
/// results in an error.
#[test]
#[should_panic(expected = "Cannot build .zkwa file: No ZKBC section provided")]
fn get_zkwa_no_zkbc() {
    // add sections
    let mut sections = Sections::new();
    let wasm_file = PathBuf::from("src/unit_tests/resources/zk_voting_simple.wasm");
    sections.add_section(WASM_IDENTIFIER_BYTE, &wasm_file);

    // read output
    sections.get_zkwa_bytes();
}

/// Reads the bytes of a resource file.
fn read_resource_file(file: &str) -> Vec<u8> {
    let mut abi_bytes = Vec::new();
    let mut abi_file = File::open(format!("src/unit_tests/resources/{}", file)).unwrap();
    abi_file.read_to_end(&mut abi_bytes).unwrap();
    abi_bytes
}
