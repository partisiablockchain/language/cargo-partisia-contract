use crate::build_command::build_package_error::BuildPackageError;

#[test]
fn debug_test() {
    let e = BuildPackageError;
    assert_eq!(format!("{e:?}"), "BuildPackageError");
}
