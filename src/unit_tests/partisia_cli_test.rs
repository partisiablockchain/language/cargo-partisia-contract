use std::{error::Error, fs::File};

use crate::{
    clap_cli::Cargo,
    constants::{CONFIG_TOML_FILE_NAME, ENV_VAR_PBC_FOLDER},
    run_command,
};
use assert_fs::TempDir;
use clap::Parser as _;
use flexi_logger::{Logger, LoggerHandle};

#[test]
fn partisia_cli_help() {
    let tempdir = TempDir::new().unwrap();
    File::create(tempdir.join(CONFIG_TOML_FILE_NAME)).unwrap();

    temp_env::with_var(
        ENV_VAR_PBC_FOLDER,
        Some(tempdir.path()),
        || -> Result<(), Box<dyn Error>> {
            let logger = Logger::try_with_str("warn")
                .unwrap()
                .do_not_log()
                .start()
                .unwrap();

            run_cli_command(
                vec!["cargo", "partisia-contract", "transaction", "--help"],
                logger.clone(),
            )?;
            run_cli_command(vec!["cargo", "pbc", "account", "--help"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "block", "--help"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "contract", "--help"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "wallet", "--help"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "abi", "--help"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "config", "--help"], logger.clone())?;

            run_cli_command(
                vec!["cargo", "pbc", "transaction", "--version"],
                logger.clone(),
            )?;
            run_cli_command(vec!["cargo", "pbc", "account", "--version"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "block", "--version"], logger.clone())?;
            run_cli_command(
                vec!["cargo", "pbc", "contract", "--version"],
                logger.clone(),
            )?;
            run_cli_command(vec!["cargo", "pbc", "wallet", "--version"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "abi", "--version"], logger.clone())?;
            run_cli_command(vec!["cargo", "pbc", "config", "--version"], logger.clone())?;

            run_cli_command(
                vec!["cargo", "pbc", "config", "--help", "--debug"],
                logger.clone(),
            )
        },
    )
    .unwrap()
}

fn run_cli_command(command: Vec<&str>, logger: LoggerHandle) -> Result<(), Box<dyn Error>> {
    let parsed = Cargo::try_parse_from(command).unwrap();
    let arguments = match parsed {
        Cargo::PartisiaContract(arguments) => arguments,
        Cargo::Pbc(arguments) => arguments,
    };
    run_command(arguments, logger)
}
