use crate::override_sdk::{parse_sdk_info, update_all_sdk_dependencies, SdkOverride, SdkVersion};
use toml_edit::DocumentMut;

#[test]
fn debug_test() {
    let sdk_override = SdkOverride {
        git: Some("my_url.git".to_string()),
        version: Some(SdkVersion::Tag {
            tag: "10.0".to_string(),
        }),
    };
    assert_eq!(
        format!("{sdk_override:?}"),
        "SdkOverride { git: Some(\"my_url.git\"), version: Some(Tag { tag: \"10.0\" }) }"
    );
}

#[test]
fn parse_sdk_info_test() {
    let sdk_override = parse_sdk_info("git: my_url.git, tag: 10.0");
    let expected = SdkOverride {
        git: Some("my_url.git".to_string()),
        version: Some(SdkVersion::Tag {
            tag: "10.0".to_string(),
        }),
    };
    assert_eq!(sdk_override, expected);

    let sdk_override = parse_sdk_info("tag: 10.0");
    let expected = SdkOverride {
        git: None,
        version: Some(SdkVersion::Tag {
            tag: "10.0".to_string(),
        }),
    };
    assert_eq!(sdk_override, expected);

    let sdk_override = parse_sdk_info("branch: my_branch");
    let expected = SdkOverride {
        git: None,
        version: Some(SdkVersion::Branch {
            branch: "my_branch".to_string(),
        }),
    };
    assert_eq!(sdk_override, expected);

    let sdk_override = parse_sdk_info("rev: 1234");
    let expected = SdkOverride {
        git: None,
        version: Some(SdkVersion::Revision {
            rev: "1234".to_string(),
        }),
    };
    assert_eq!(sdk_override, expected);

    let sdk_override = parse_sdk_info("git: my_url.git");
    let expected = SdkOverride {
        git: Some("my_url.git".to_string()),
        version: None,
    };
    assert_eq!(sdk_override, expected);
}

#[test]
#[should_panic(
    expected = "Only one tag, branch, or rev is allowed when overriding the sdk, got tags: [\"10.0\"], branches: [\"main\"], revs: []"
)]
fn parse_sdk_info_multiple_versions() {
    parse_sdk_info("git: my_url.git, tag: 10.0, branch: main");
}

#[test]
#[should_panic(
    expected = "Only one git url is allowed when overriding the sdk, got [\"url1\", \"url2\"]"
)]
fn parse_sdk_info_multiple_urls() {
    parse_sdk_info("git: url1, git: url2");
}

#[test]
#[should_panic(expected = "Invalid sdk argument, got some string")]
fn parse_sdk_info_wrong_input() {
    parse_sdk_info("some string");
}

#[test]
fn update_dependency_test() {
    let sdk_override = parse_sdk_info("git: my_url.git, tag: 10.0");
    let original = r#"
    [package]
    name = "simple"
    version = "0.1.0"
    edition = "2015"

    # Comment outside

    [dependencies]
    pbc_contract_common = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", tag = "11.0.0" } #Comment inside dependency table
    unrelated_dependency = { git = "some_url.git" }
    serde_json = "1.0"

    [target.wasm32-unknown-unknown.dependencies]
    pbc_traits = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", tag = "11.0.0" }
    pbc_lib = { git = "weirdurl.git", tag = "11.0.0" }
    read_write_rpc_derive = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", branch = "main" }
    read_write_state_derive = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", rev = "6798234" }

    [dev-dependencies]
    pbc_contract_codegen = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", tag = "11.0.0" }

    [build-dependencies]
    pbc_contract_codegen = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", tag = "10.0.0" }

    [target.wasm32-unknown-unknown.dev-dependencies]
    create_type_spec_derive = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", tag = "9.0.0" }

    [target.wasm32-unknown-unknown.build-dependencies]
    create_type_spec_derive = { git = "https://git@gitlab.com/partisiablockchain/language/contract-sdk.git", tag = "8.0.0" }
    "#;
    let expected = r#"
    [package]
    name = "simple"
    version = "0.1.0"
    edition = "2015"

    # Comment outside

    [dependencies]
    pbc_contract_common = { git = "my_url.git", tag = "10.0" } #Comment inside dependency table
    unrelated_dependency = { git = "some_url.git" }
    serde_json = "1.0"

    [target.wasm32-unknown-unknown.dependencies]
    pbc_traits = { git = "my_url.git", tag = "10.0" }
    pbc_lib = { git = "my_url.git", tag = "10.0" }
    read_write_rpc_derive = { git = "my_url.git", tag = "10.0" }
    read_write_state_derive = { git = "my_url.git", tag = "10.0" }

    [dev-dependencies]
    pbc_contract_codegen = { git = "my_url.git", tag = "10.0" }

    [build-dependencies]
    pbc_contract_codegen = { git = "my_url.git", tag = "10.0" }

    [target.wasm32-unknown-unknown.dev-dependencies]
    create_type_spec_derive = { git = "my_url.git", tag = "10.0" }

    [target.wasm32-unknown-unknown.build-dependencies]
    create_type_spec_derive = { git = "my_url.git", tag = "10.0" }
    "#;

    let mut document: DocumentMut = original.parse::<DocumentMut>().unwrap();

    update_all_sdk_dependencies(&mut document, &sdk_override);

    assert_eq!(document.to_string(), expected);
}

#[test]
fn update_dependency_does_not_add() {
    let sdk_override = parse_sdk_info("git: my_url.git, tag: 10.0");
    let original = r#"
    [package]
    name = "simple"
    version = "0.1.0"
    edition = "2015"

    # Comment outside

    [dependencies]
    "#;
    let expected = r#"
    [package]
    name = "simple"
    version = "0.1.0"
    edition = "2015"

    # Comment outside

    [dependencies]
    "#;

    let mut document: DocumentMut = original.parse::<DocumentMut>().unwrap();

    update_all_sdk_dependencies(&mut document, &sdk_override);

    assert_eq!(document.to_string(), expected);
}

#[test]
fn dependency_in_toml_not_table_like() {
    let sdk_override = parse_sdk_info("git: my_url.git, tag: 10.0");
    let original = r#"
    dependencies = "weird"
    "#;
    let expected = r#"
    dependencies = "weird"
    "#;

    let mut document: DocumentMut = original.parse::<DocumentMut>().unwrap();

    update_all_sdk_dependencies(&mut document, &sdk_override);

    assert_eq!(document.to_string(), expected);
}
