use crate::get_compiler::get_file_http;
use assert_fs::TempDir;

#[test]
fn get_file_http_invalid_save_path_test() {
    let mut server = mockito::Server::new();
    let _mock = server
        .mock("GET", "/test.jar")
        .with_body("hello")
        .with_header("content-type", "application/octet-stream")
        .create();

    let tempdir = TempDir::new().unwrap();
    let save_path = tempdir.join("non-existent").join("invalid.jar");
    let url = server.url() + "test.jar";
    let res = get_file_http(&url, &save_path);
    assert!(res.is_err());
}
