use crate::constants::{
    ABI_CLI, CONFIG_TOML_FILE_NAME, DEFAULT_PBC_FOLDER_NAME, ENV_VAR_PBC_FOLDER,
};
use crate::jar_command::{get_fetch_metadata_or_default, pbc_folder, JarCommandError};
use assert_fs::TempDir;
use dirs::home_dir;
use std::fs::File;
use std::io::Write;

#[test]
fn debug_test() {
    let e = JarCommandError::from(None);
    assert_eq!(format!("{e:?}"), "AbiCliError");

    let e = JarCommandError::from(Some("hello".to_string()));
    assert_eq!(format!("{e:?}"), "AbiCliError");

    let e = JarCommandError::from_str("123");
    assert_eq!(format!("{e:?}"), "AbiCliError");
}

#[test]
fn display_test() {
    let e = JarCommandError::from(Some(ABI_CLI.to_string()));
    assert_eq!(
        format!("{e}"),
        "abi-cli jar command failed, see console output for more details."
    );

    let e = JarCommandError::from_str("hello world");
    assert_eq!(
        format!("{e}"),
        "hello world jar command failed, see console output for more details."
    );

    let e = JarCommandError::from(None);
    assert_eq!(
        format!("{e}"),
        "Jar command failed, see console output for more details."
    );
}

#[test]
#[should_panic(expected = "Expected url in ~/.pbc/config.toml to be a String")]
fn get_fetch_metadata_non_string_url_test() {
    let tempdir = TempDir::new().unwrap();
    let config = "package = { metadata = { abi-cli = { url = ['test'] } } }";
    let mut file = File::create(tempdir.join(CONFIG_TOML_FILE_NAME)).unwrap();
    file.write_all(config.as_bytes()).unwrap();

    temp_env::with_var(ENV_VAR_PBC_FOLDER, Some(tempdir.path()), || {
        get_fetch_metadata_or_default(ABI_CLI);
    });
}

#[test]
fn get_fetch_metadata_no_url_test() {
    let tempdir = TempDir::new().unwrap();
    let config = "package = { metadata = { abi-cli = { something-else = 'test' } } }";
    let mut file = File::create(tempdir.join(CONFIG_TOML_FILE_NAME)).unwrap();
    file.write_all(config.as_bytes()).unwrap();

    temp_env::with_var(ENV_VAR_PBC_FOLDER, Some(tempdir.path()), || {
        get_fetch_metadata_or_default(ABI_CLI);
    });
}

#[test]
#[should_panic(expected = "Invalid config file,  must be on a valid toml format")]
fn get_fetch_metadata_non_toml_test() {
    let tempdir = TempDir::new().unwrap();
    let mut file = File::create(tempdir.join(CONFIG_TOML_FILE_NAME)).unwrap();
    file.write_all(b"Hello world").unwrap();

    temp_env::with_var(ENV_VAR_PBC_FOLDER, Some(tempdir.path()), || {
        get_fetch_metadata_or_default(ABI_CLI);
    });
}

#[test]
fn pbc_folder_env_var_test() {
    let tempdir = TempDir::new().unwrap();
    temp_env::with_var(ENV_VAR_PBC_FOLDER, Some(tempdir.path()), || {
        let pbc_folder = pbc_folder();
        assert_eq!(pbc_folder, tempdir.path());
    });
}

#[test]
fn pbc_folder_test() {
    temp_env::with_var_unset(ENV_VAR_PBC_FOLDER, || {
        let pbc_folder = pbc_folder();
        let expected = home_dir().unwrap().join(DEFAULT_PBC_FOLDER_NAME);
        assert_eq!(pbc_folder, expected);
    });
}
