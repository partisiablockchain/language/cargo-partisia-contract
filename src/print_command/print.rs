use crate::get_versions;
use std::error::Error;
use std::path::Path;

pub fn print_version(bashlike: &bool, wasm: &String) -> Result<(), Box<dyn Error>> {
    let wasm_file = Path::new(wasm);
    let versions = get_versions(wasm_file, &None)?;

    if *bashlike {
        println!("version_binder={:?}", versions.binder());
        println!("version_client={:?}", versions.client());
    } else {
        println!("Binder version = {}", versions.binder());
        println!("Client version = {}", versions.client());
    }
    Ok(())
}
