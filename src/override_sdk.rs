use crate::constants::OVERRIDE_SDK_SAVE_TOML_TARGET_PATH;
use crate::override_sdk::SdkVersion::{Branch, Revision, Tag};
use cargo_metadata::Package;
use std::fs;
use std::path::PathBuf;
use toml_edit::{DocumentMut, Table};

/// Options for overriding the sdk.
#[derive(PartialEq, Eq, Debug)]
pub(crate) struct SdkOverride {
    /// if set used for overriding the git url of the sdk.
    pub(crate) git: Option<String>,
    /// if set used for overriding the tag/branch/rev of the sdk.
    pub(crate) version: Option<SdkVersion>,
}

/// The different ways the version of the sdk can be overridden.
#[derive(PartialEq, Eq, Debug)]
pub(crate) enum SdkVersion {
    /// `tag`: [`String`], if set used for overriding the tag of the sdk.
    Tag {
        /// The tag
        tag: String,
    },
    /// `branch`: [`String`], if set used for overriding the branch of the sdk.
    Branch {
        /// The branch
        branch: String,
    },
    /// `rev`: [`String`], if set used for overriding the revision of the sdk.
    Revision {
        /// The revision
        rev: String,
    },
}

/// Overrides the sdk for the given list of packages.
/// The sdk needs to be a comma separated string each entry being either git:`<url>`, tag:`<tag>`, branch:`<branch>`, or rev:`<rev>`.
/// If only the git url is given then the command keeps the tag/branch/rev from the previous sdk.
///
/// ### Params:
/// * `sdk`: [`&str`], the comma separated string describing the new sdk.
/// * `contracts`: [`&[Package]`] the list of packages that will be updated.
/// * `target_folder`: [`PathBuf`], the relative path to the target folder for the project.
///
pub(crate) fn override_sdk(sdk: &str, contracts: &[Package], target_folder: PathBuf) {
    for contract in contracts {
        let manifest_data =
            fs::read_to_string(&contract.manifest_path).expect("Failed to read manifest contents");
        let mut saved_toml_path = PathBuf::from(&target_folder);
        saved_toml_path.push(OVERRIDE_SDK_SAVE_TOML_TARGET_PATH);
        fs::create_dir_all(&saved_toml_path)
            .expect("Failed to create directory for copy of Cargo.toml");
        saved_toml_path.push(format!("{}.toml", &contract.name));
        fs::write(saved_toml_path, manifest_data.as_bytes())
            .expect("Failed to save copy of Cargo.toml");
    }

    let sdk_override = parse_sdk_info(sdk);

    for contract in contracts {
        let manifest_data =
            fs::read_to_string(&contract.manifest_path).expect("Failed to read manifest contents");
        let mut manifest: DocumentMut = manifest_data.parse().expect("Unable to parse Cargo.toml");

        update_all_sdk_dependencies(&mut manifest, &sdk_override);

        let s = manifest.to_string();
        let new_contents_bytes = s.as_bytes();

        fs::write(&contract.manifest_path, new_contents_bytes)
            .expect("Failed to write updated Cargo.toml");
    }
}

/// Updates the sdk dependencies in the given toml document.
/// The sdk needs to be a comma separated string each entry being either git:`<url>`, tag:`<tag>`, branch:`<branch>`, or rev:`<rev>`.
/// If only the git url is given then the command keeps the tag/branch/rev from the previous sdk.
///
/// ### Params:
/// * `manifest`: &mut [`DocumentMut`], a pointer to the mutable manifest toml document.
/// * `sdk_override`: &[`SdkOverride`] the description of the new version of the sdk.
///
pub(crate) fn update_all_sdk_dependencies(manifest: &mut DocumentMut, sdk_override: &SdkOverride) {
    let sdk_dependency_keys = [
        "pbc_contract_common",
        "pbc_traits",
        "pbc_lib",
        "read_write_rpc_derive",
        "read_write_state_derive",
        "create_type_spec_derive",
        "pbc_contract_codegen",
        "pbc_zk",
    ];

    let update_all_sdk_dependencies_in_section = |dependency_table: &mut Table| {
        for &sdk_dependency_key in sdk_dependency_keys.iter() {
            update_dependency(sdk_dependency_key, sdk_override, dependency_table);
        }
    };

    apply_to_sections(manifest, update_all_sdk_dependencies_in_section);
}

fn apply_to_sections<F>(manifest: &mut DocumentMut, f: F)
where
    F: Fn(&mut Table),
{
    // Dependencies can be in the three standard sections...
    for dependency_type in ["dependencies", "dev-dependencies", "build-dependencies"] {
        if manifest
            .get(dependency_type)
            .map(|t| t.is_table_like())
            .unwrap_or(false)
        {
            if let Some(dependency_table) = manifest[dependency_type].as_table_mut() {
                f(dependency_table);
            }
        }

        // ... and in `target.<target>.(build-/dev-)dependencies`.
        manifest
            .get_mut("target")
            .and_then(toml_edit::Item::as_table_like_mut)
            .into_iter()
            .flat_map(toml_edit::TableLike::iter_mut)
            .for_each(|(_, target_table)| {
                if let Some(dependency_table) = target_table[dependency_type].as_table_mut() {
                    f(dependency_table);
                }
            });
    }
}

fn update_dependency(
    dependency_key: &str,
    sdk_override: &SdkOverride,
    dependency_table: &mut Table,
) {
    if let Some(table) = dependency_table
        .get_mut(dependency_key)
        .and_then(|item| item.as_table_like_mut())
    {
        if let Some(git) = &sdk_override.git {
            table.insert("git", toml_edit::value(git));
        }
        if let Some(version) = &sdk_override.version {
            match version {
                Tag { tag } => {
                    table.insert("tag", toml_edit::value(tag));
                    table.remove("branch");
                    table.remove("rev");
                }
                Branch { branch } => {
                    table.insert("branch", toml_edit::value(branch));
                    table.remove("tag");
                    table.remove("rev");
                }
                Revision { rev } => {
                    table.insert("rev", toml_edit::value(rev));
                    table.remove("tag");
                    table.remove("branch");
                }
            }
        }
        for key in ["version", "path", "registry"] {
            table.remove(key);
        }
        table.fmt();
    }
}

/// Parse the sdk information given to a command to get the optional git url and the version.
/// The version can either be a tag, a branch, or a revision.
/// Panics if multiple versions or git urls are set.
///
/// ### Parameters:
///
/// * `sdk_option`: [`&str`], the sdk argument given to the command.
///
/// ### Returns:
/// An [`SdkOverride`] containing the optional git url and version.
pub(crate) fn parse_sdk_info(sdk: &str) -> SdkOverride {
    let override_values = sdk.split(',');
    let git_urls: Vec<String> = override_values
        .clone()
        .flat_map(|value| {
            value
                .trim()
                .strip_prefix("git:")
                .map(|git| git.trim().to_string())
        })
        .collect();
    let tags: Vec<String> = override_values
        .clone()
        .flat_map(|value| {
            value
                .trim()
                .strip_prefix("tag:")
                .map(|tag| tag.trim().to_string())
        })
        .collect();
    let branches: Vec<String> = override_values
        .clone()
        .flat_map(|value| {
            value
                .trim()
                .strip_prefix("branch:")
                .map(|branch| branch.trim().to_string())
        })
        .collect();
    let revs: Vec<String> = override_values
        .flat_map(|value| {
            value
                .trim()
                .strip_prefix("rev:")
                .map(|rev| rev.trim().to_string())
        })
        .collect();
    if git_urls.len() > 1 {
        panic!("Only one git url is allowed when overriding the sdk, got {git_urls:?}");
    } else if tags.len() + branches.len() + revs.len() > 1 {
        panic!("Only one tag, branch, or rev is allowed when overriding the sdk, got tags: {tags:?}, branches: {branches:?}, revs: {revs:?}");
    } else if git_urls.is_empty() && tags.is_empty() && branches.is_empty() && revs.is_empty() {
        panic!("Invalid sdk argument, got {sdk}");
    } else {
        let sdk_version: Option<SdkVersion>;
        if !tags.is_empty() {
            sdk_version = Some(Tag {
                tag: tags.first().unwrap().clone(),
            });
        } else if !branches.is_empty() {
            sdk_version = Some(Branch {
                branch: branches.first().unwrap().clone(),
            });
        } else if !revs.is_empty() {
            sdk_version = Some(Revision {
                rev: revs.first().unwrap().clone(),
            });
        } else {
            sdk_version = None
        }
        SdkOverride {
            git: git_urls.first().cloned(),
            version: sdk_version,
        }
    }
}
