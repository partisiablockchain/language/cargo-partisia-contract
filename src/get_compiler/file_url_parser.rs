use crate::get_compiler::file_url_parser::Url::GenericUrl;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

/// Enum for the different kinds of supported Url.
#[derive(Debug, PartialEq)]
pub(crate) enum Url {
    /// A generic url, e.g. `https://gitlab.com`
    GenericUrl { url: String },
}

/// Error for parsing an url, e.g. if given a file url with host.
#[derive(Debug, PartialEq, Eq)]
pub(crate) struct ParseUrlError {
    /// The invalid url.
    pub(crate) url: String,
}

impl Display for ParseUrlError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let err = format!("{} is not a supported Url.", self.url);
        write!(f, "{err}")
    }
}

impl Error for ParseUrlError {}

impl FromStr for Url {
    type Err = ParseUrlError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        Ok(GenericUrl {
            url: value.to_string(),
        })
    }
}
