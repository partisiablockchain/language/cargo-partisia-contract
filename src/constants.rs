//! Constants used in the cargo partisia contract package.

/// Environment variable to specify the path to the PBC_FOLDER where
/// downloaded jars, as well as the config.toml is stored.
pub const ENV_VAR_PBC_FOLDER: &str = "PBC_FOLDER";

/// The default location for the pbc folder.
pub const DEFAULT_PBC_FOLDER_NAME: &str = ".pbc";

/// The filename for the config.toml stored in the pbc folder.
pub const CONFIG_TOML_FILE_NAME: &str = "config.toml";

/// Default artifact name for the abi client.
pub const ABI_CLI: &str = "abi-cli";

/// Default artifact name for the partisia-cli.
pub const PARTISIA_CLI: &str = "partisia-cli";

/// The path in target where the previous Cargo.toml for each contract package will be saved
/// while running with the new sdk version. This is then read later on to reset the Cargo.toml.
pub const OVERRIDE_SDK_SAVE_TOML_TARGET_PATH: &str = "saved_cargo_toml";
