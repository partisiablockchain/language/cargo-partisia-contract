use regex::Regex;
use std::error::Error;
use std::fmt;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub(crate) struct WasmFileError;

impl Display for WasmFileError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "Could not locate the CLIENT/BINDER version in the WASM file."
        )
    }
}

impl Error for WasmFileError {}

pub(crate) struct Versions {
    pub(crate) binder: [u8; 3],
    pub(crate) client: [u8; 3],
}

impl Versions {
    fn semver_format(version: &[u8; 3]) -> String {
        format!("{}.{}.{}", version[0], version[1], version[2])
    }

    pub(crate) fn client(&self) -> String {
        Versions::semver_format(&self.client)
    }

    pub(crate) fn binder(&self) -> String {
        Versions::semver_format(&self.binder)
    }

    pub(crate) fn regex_binder() -> Regex {
        Versions::regex_from_prefix("__PBC_VERSION_BINDER")
    }

    pub(crate) fn regex_client() -> Regex {
        Versions::regex_from_prefix("__PBC_VERSION_CLIENT")
    }

    fn regex_from_prefix(prefix: &str) -> Regex {
        let regex_string = format!(r"{prefix}_(\d+)_(\d+)_(\d+)");
        Regex::new(&regex_string).unwrap()
    }

    pub(crate) fn extract_version_from_string(string: String, regex: &Regex) -> [u8; 3] {
        let captures = regex.captures(&string);
        let capture = captures.iter().next().unwrap();

        let major: u8 = (capture[1]).parse().unwrap();
        let minor: u8 = (capture[2]).parse().unwrap();
        let patch: u8 = (capture[3]).parse().unwrap();

        [major, minor, patch]
    }

    /// Find export that matches regex
    ///
    /// ### Parameters:
    /// * `exports` [`&[&str]`] List of exported variables from wasm file.
    /// * `regex` [`Regex`] Regex for matching version.
    ///
    /// ### Returns:
    /// Export that matches regex
    fn find_version_by_regex(exports: &[&str], regex: &Regex) -> Result<String, Box<dyn Error>> {
        for export in exports {
            if regex.is_match(export) {
                return Ok(export.to_string());
            }
        }
        Err(WasmFileError.into())
    }

    /// Extract version with regex from list of exported variables in wasm file.
    ///
    /// ### Parameters:
    /// * `exports` [`&[&str]`] List of exported variables from wasm file.
    /// * `regex` [`Regex`] Regex for matching version.
    ///
    /// ### Returns:
    /// The version extracted from the exports list
    fn extract_version_from_exports(
        exports: &[&str],
        regex: Regex,
    ) -> Result<[u8; 3], Box<dyn Error>> {
        let version = Versions::find_version_by_regex(exports, &regex)?;
        Ok(Versions::extract_version_from_string(version, &regex))
    }

    /// Extract the client and binder verison from list of wasm exports.
    ///
    /// ### Parameters:
    /// * `exports` [`&[&str]`], List of wasm exports
    ///
    /// ### Returns:
    /// The binder and client version as [`Versions`].
    pub(crate) fn from_wasm_exports(exports: &[&str]) -> Result<Versions, Box<dyn Error>> {
        let client = Versions::extract_version_from_exports(exports, Versions::regex_client())?;
        let binder = Versions::extract_version_from_exports(exports, Versions::regex_binder())?;

        Ok(Versions { binder, client })
    }
}
