//! This is the cargo sub command for compiling a WASM contract on the Partisia Blockchain.

use std::error::Error;
use std::io;
use std::io::Write;
use std::path::Path;
use std::process::ExitCode;

use clap::Parser;
use flexi_logger::{style, DeferredNow, FlexiLoggerError, LogSpecification, Logger, LoggerHandle};
use log::{error, LevelFilter, Record};

use crate::build_command::builder::BuildCommandBuilder;

use crate::package_handler::utils::{
    file_in_target, get_contracts, get_metadata, get_packages, get_root_package,
    get_target_folder_metadata,
};

use crate::clap_cli::{Arguments, Cargo, Commands};
use crate::constants::ABI_CLI;
use crate::get_compiler::{get_zk_compute_paths, retrieve_jar, retrieve_zk_compiler};
use crate::jar_command::CallableJar;
use crate::override_sdk::override_sdk;
use crate::partisia_cli_command::partisia_cli::PartisiaCli;
use crate::print_command::print::print_version;
use crate::version::Versions;

/// Building contracts and non-contracts.
mod build_command;
/// The cli.
mod clap_cli;
/// Constants.
pub(crate) mod constants;
mod get_compiler;
/// Calling jars.
mod jar_command;
/// module for --sdk argument to build
mod override_sdk;
/// Module containing utility methods
mod package_handler;
/// Handling the partisia cli command.
mod partisia_cli_command;
/// Module for printing
mod print_command;
/// Test module
#[cfg(test)]
mod unit_tests;
mod version;

mod wasm_gc;

/// The main function for running the Cargo::PartisiaContract commands.
fn main() -> ExitCode {
    let logger_handle: LoggerHandle = start_logger().unwrap();

    let arguments = match Cargo::parse() {
        Cargo::PartisiaContract(arguments) => arguments,
        Cargo::Pbc(arguments) => arguments,
    };
    run_command(arguments, logger_handle).handle()
}

/// Function for defining what happens when each command is called.
///
/// ### Parameters:
///
/// * `arguments`: [`Arguments`], the command itself as well as its arguments.
///
/// ### Returns:
///
/// An empty result or a heap allocated Error.
fn run_command(arguments: Arguments, logger_handle: LoggerHandle) -> Result<(), Box<dyn Error>> {
    match &arguments.commands {
        Commands::Init {
            workspace,
            manifest_path,
        } => {
            let metadata = get_metadata(manifest_path)?;
            let packages = get_packages(&metadata, *workspace, vec![])?;
            for package in packages {
                if (get_zk_compute_paths(package)?).is_some() {
                    println!("Initializing {} ", package.name);
                    retrieve_zk_compiler(false, package, &metadata)?;
                    println!();
                }
            }
        }
        Commands::Build {
            release,
            no_abi,
            quiet,
            no_wasm_strip,
            no_zk,
            disable_git_fetch_with_cli,
            workspace,
            manifest_path,
            coverage,
            package,
            additional_args,
        } => {
            if *quiet {
                let warn_spec = LogSpecification::builder()
                    .default(LevelFilter::Warn)
                    .build();
                logger_handle.set_new_spec(warn_spec);
            }

            let build_command_from_builder = BuildCommandBuilder::builder()
                .set_release(*release)
                .set_no_abi(*no_abi)
                .set_quiet(*quiet)
                .set_no_wasm_strip(*no_wasm_strip)
                .set_no_zk(*no_zk)
                .set_disable_git_fetch_with_cli(*disable_git_fetch_with_cli)
                .set_manifest_path(manifest_path.clone())
                .set_workspace(*workspace)
                .set_coverage(*coverage)
                .set_packages(package.clone())
                .set_additional_args(additional_args.clone())
                .build_command()?;

            build_command_from_builder.start_build()?;
        }
        Commands::PrintVersion { bashlike, wasm } => print_version(bashlike, wasm)?,
        Commands::PathOfWasm {
            release,
            manifest_path,
        } => {
            let metadata = get_metadata(manifest_path)?;
            let package = get_root_package(&metadata);
            let target_folder = get_target_folder_metadata(&metadata);
            println!(
                "{}",
                file_in_target(&package.name, *release, "wasm", &target_folder).to_string_lossy()
            );
        }
        Commands::PathOfAbi {
            release,
            manifest_path,
        } => {
            let metadata = get_metadata(manifest_path)?;
            let package = get_root_package(&metadata);
            let target_folder = get_target_folder_metadata(&metadata);
            println!(
                "{}",
                file_in_target(&package.name, *release, "abi", &target_folder).to_string_lossy()
            );
        }
        Commands::SetSdk {
            sdk,
            workspace,
            manifest_path,
            package,
        } => {
            let metadata = get_metadata(manifest_path)?;
            let packages = get_packages(&metadata, *workspace, package.clone())?;
            let contracts = get_contracts(packages.clone())?;
            override_sdk(sdk, &contracts, get_target_folder_metadata(&metadata));
        }
        Commands::Transaction { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.transaction(args)?
            }
        }
        Commands::Account { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.account(args)?
            }
        }
        Commands::Wallet { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.wallet(args)?
            }
        }
        Commands::Contract { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.contract(args)?
            }
        }
        Commands::Config { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.config(args)?
            }
        }
        Commands::Block { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.block(args)?
            }
        }
        Commands::Abi { cli_args } => {
            let cli = PartisiaCli::new(cli_args.url.clone(), cli_args.help);
            let args = cli_args.additional_args.clone();
            if cli_args.version {
                cli.print_version()?
            } else {
                cli.abi(args)?
            }
        }
    }
    Ok(())
}

trait HandleError {
    /// Helper function for displaying an error if present and then exiting the program.
    fn handle(self) -> ExitCode;
}

impl HandleError for Result<(), Box<dyn Error>> {
    fn handle(self) -> ExitCode {
        match self {
            Err(e) => {
                error!("{}", e);
                let mut source = e.source();
                while let Some(source_error) = source {
                    error!("caused by: {}", source_error);
                    source = source_error.source();
                }
                ExitCode::FAILURE
            }
            Ok(_) => ExitCode::SUCCESS,
        }
    }
}

/// Extract the binder and client version of the given wasm contract. Returns an error if the given
/// path does not point to a valid wasm contract.
///
/// ### Parameters:
///
/// * `wasm_file`: &[`Path`], path to the wasm_file to get the version from.
///
/// ### Returns:
///
/// A result containing the binder and client version of the WasmContract.
pub(crate) fn get_versions(
    wasm_file: &Path,
    abi_cli_url: &Option<String>,
) -> Result<Versions, Box<dyn Error>> {
    let abi_client = CallableJar::from(retrieve_jar(abi_cli_url, ABI_CLI)?);
    abi_client.save_jar_fetch_config(ABI_CLI)?;
    let output = abi_client.call_jar(
        &vec![
            "wasm-exports".to_string(),
            wasm_file.to_string_lossy().to_string(),
        ],
        Some(std::process::Stdio::piped()),
        Some("Unable to read versions from wasm file".to_string()),
    )?;

    let stdout = String::from_utf8(output.stdout)?;
    let exports: Vec<&str> = stdout.lines().collect();

    let versions = Versions::from_wasm_exports(&exports)?;
    Ok(versions)
}

fn start_logger() -> Result<LoggerHandle, FlexiLoggerError> {
    let specification = LogSpecification::builder()
        .default(LevelFilter::Info)
        .build();
    let logger = Logger::with(specification);
    logger
        .log_to_stdout()
        .format_for_stdout(custom_format)
        .set_palette("9;11;12".to_string())
        .start()
}

/// Custom log message format for only logging the level and the message.
///
/// ### Parameters:
///
/// * `writer`: &mut dyn [`Write`], writer to write the message to.
///
/// * `_now`: &mut [`DeferredNow`], time stamp, not used in this format.
///
/// * `record`: &[`Record`], record from the call to the logger, providing the message and other information.
pub(crate) fn custom_format(
    writer: &mut dyn Write,
    _now: &mut DeferredNow,
    record: &Record,
) -> Result<(), io::Error> {
    let level = record.level();
    write!(
        writer,
        "{} {}",
        style(level).paint(level.to_string()),
        record.args()
    )
}
