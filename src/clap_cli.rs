use clap::{Args, Parser, Subcommand};

#[derive(Parser)]
#[clap(name = "cargo", bin_name = "cargo")]
pub(crate) enum Cargo {
    PartisiaContract(Arguments),
    Pbc(Arguments),
}

#[derive(clap::Args)]
#[clap(
    author,
    version,
    long_about = "\nPartisia-contract \nCompiles Smart Contracts for the Partisia Blockchain to WASM- and ABI-files for deployment on-chain.",
    about = "\nPartisia-contract \nCompiles Smart Contracts for the Partisia Blockchain to WASM- and ABI-files for deployment on-chain."
)]
pub(crate) struct Arguments {
    #[clap(subcommand)]
    pub(crate) commands: Commands,
}

#[derive(Args, Clone)]
#[clap(disable_help_flag = true)]
pub struct CliArgs {
    #[clap(help = "No messages printed to stdout", short = 'q', long = "quiet")]
    quiet: bool,
    #[clap(
        help = "Debug messages printed to stdout",
        short = 'v',
        long = "verbose"
    )]
    verbose: bool,
    #[clap(
        help = "Url specifying the location to retrieve the partisia-cli JAR from. If not given, a user configuration file in\n\
                    ~/.pbc/config.toml or default values will be used.\n\
                    Uses netrc for authentication.\n\
                    Example usage:\n\
                     --use https://gitlab.com/api/v4/groups/12499775/-/packages/maven/com/partisiablockchain/language/partisia-cli/4.1.0/partisia-cli-4.1.0-jar-with-dependencies.jar",
        short = 'u',
        long = "use"
    )]
    pub(crate) url: Option<String>,
    #[clap(
        help = "Print usage description of the command.",
        short = 'h',
        long = "help"
    )]
    pub(crate) help: bool,
    #[clap(
        help = "Print version of the Partisia Cli",
        short = 'V',
        long = "version"
    )]
    pub(crate) version: bool,
    #[clap(
        help = "Additional arguments that will be passed along to partisia-cli.jar",
        num_args = 1..,
        allow_hyphen_values = true
    )]
    pub(crate) additional_args: Vec<String>,
}

#[derive(Subcommand)]
pub(crate) enum Commands {
    #[clap(
        about = "Compile contracts to WASM and generate ABI files.",
        trailing_var_arg = true
    )]
    Build {
        #[clap(
            help = "Build artifacts in release mode, with optimizations",
            short = 'r',
            long = "release"
        )]
        release: bool,
        #[clap(help = "Skip generating .abi file", short = 'n', long = "no-abi")]
        no_abi: bool,
        #[clap(help = "No messages printed to stdout", short = 'q', long = "quiet")]
        quiet: bool,
        #[clap(
            help = "Do not remove custom sections from the WASM-file (will produce a much larger file).",
            short = 'w',
            long = "no-wasm-strip"
        )]
        no_wasm_strip: bool,
        #[clap(
            help = "Only compile the public part of the contract. Skips compilation of ZK computation.",
            short = 'z',
            long = "no-zk"
        )]
        no_zk: bool,
        #[clap(
            help = "Uses cargo's built-in git library to fetch dependencies instead of the git executable",
            long = "disable-git-fetch-with-cli"
        )]
        disable_git_fetch_with_cli: bool,
        #[clap(help = "Build all packages in the workspace", long = "workspace")]
        workspace: bool,
        #[clap(
            help = "Specify path to the Cargo.toml of the contract or workspace",
            long = "manifest-path"
        )]
        manifest_path: Option<String>,
        #[clap(
            help = "Compile an instrumented binary for the smart contract. This enables generation of coverage files.",
            long = "coverage"
        )]
        coverage: bool,
        #[clap(
            help = "Build only the specified packages",
            short = 'p',
            long = "package"
        )]
        package: Vec<String>,
        #[clap(
        help = "Additional arguments that will be passed along to cargo build, \n\
            see cargo build --help for details.",
        num_args = 1..,
        allow_hyphen_values = true
        )]
        additional_args: Vec<String>,
    },
    #[clap(about = "Initialize the contract. Retrieves dependencies for build.")]
    Init {
        #[clap(help = "Init all zk contracts in the workspace", long = "workspace")]
        workspace: bool,
        #[clap(
            help = "Specify path to the Cargo.toml of the contract or workspace",
            long = "manifest-path"
        )]
        manifest_path: Option<String>,
    },
    #[clap(about = "Print the client and binder version of the contract.")]
    PrintVersion {
        #[clap(
            help = "Print the version as bash variables",
            short = 'b',
            long = "bashlike"
        )]
        bashlike: bool,

        #[clap(help = "The wasm file to load", name = "WASM contract")]
        wasm: String,
    },
    #[clap(about = "Print the expected WASM file path based on the context of Cargo.toml")]
    PathOfWasm {
        #[clap(
            help = "File is in release folder instead of debug",
            short = 'r',
            long = "release"
        )]
        release: bool,
        #[clap(
            help = "Specify path to the Cargo.toml of the contract or workspace",
            long = "manifest-path"
        )]
        manifest_path: Option<String>,
    },
    #[clap(about = "Print the expected ABI file path based on the context of Cargo.toml")]
    PathOfAbi {
        #[clap(
            help = "File is in release folder instead of debug",
            short = 'r',
            long = "release"
        )]
        release: bool,
        #[clap(
            help = "Specify path to the Cargo.toml of the contract or workspace",
            long = "manifest-path"
        )]
        manifest_path: Option<String>,
    },
    #[clap(about = "Update the sdk used for compiling the contracts. ")]
    SetSdk {
        #[clap(
            help = "The new sdk value. Git url and tag/branch/rev can be supplied at the same time or separately.\n\
                    Example usage:\n\
                    set-sdk \"git: https://git@gitlab.com/partisiablockchain/language/contract-sdk.git, tag: 9.1.2\"\n\
                    set-sdk \"branch: example_branch\"\n\
                    set-sdk \"rev: 55061d796e5547e3cdf637407d928f95e2e32c59\"",
            name = "sdk"
        )]
        sdk: String,
        #[clap(
            help = "Set the sdk for all packages in the workspace",
            long = "workspace"
        )]
        workspace: bool,
        #[clap(
            help = "Specify path to the Cargo.toml of the contract or workspace",
            long = "manifest-path"
        )]
        manifest_path: Option<String>,
        #[clap(
            help = "Build only the specified packages",
            short = 'p',
            long = "package"
        )]
        package: Vec<String>,
    },
    #[clap(
        about = "Sign, Send and interact with the Partisia Blockchain.",
        trailing_var_arg = true
    )]
    Transaction {
        #[command(flatten)]
        cli_args: CliArgs,
    },
    #[clap(
        about = "Create and interact with accounts on the Partisia Blockchain.",
        trailing_var_arg = true
    )]
    Account {
        #[command(flatten)]
        cli_args: CliArgs,
    },
    #[clap(
        about = "Get information about contracts deployed on the Partisia Blockchain.",
        trailing_var_arg = true
    )]
    Contract {
        #[command(flatten)]
        cli_args: CliArgs,
    },
    #[clap(
        about = "Set default values for options used during execution of commands.",
        trailing_var_arg = true
    )]
    Config {
        #[command(flatten)]
        cli_args: CliArgs,
    },
    #[clap(about = "View latest or specific blocks", trailing_var_arg = true)]
    Block {
        #[command(flatten)]
        cli_args: CliArgs,
    },
    #[clap(
        about = "Create a wallet that can be used for sending, signing, and interacting with the Partisia Blockchain",
        trailing_var_arg = true
    )]
    Wallet {
        #[command(flatten)]
        cli_args: CliArgs,
    },
    #[clap(
        about = "View information about an abi and generate abi code",
        trailing_var_arg = true
    )]
    Abi {
        #[command(flatten)]
        cli_args: CliArgs,
    },
}
