use crate::get_compiler::get_zk_compute_paths;
use crate::package_handler::utils::file_in_target;
use cargo_metadata::Package;
use std::error::Error;
use std::path::{Path, PathBuf};

/// Builder for `ContractPaths`
pub struct ContractPathsBuilder {
    /// The package to build.
    pub(crate) package: Package,
    /// Path to the target folder.
    pub(crate) target_folder: PathBuf,
    /// If true, the contract is built with release and placed accordingly.
    release: bool,
    /// If false, does not compile the zk part of the contracts.
    zk: bool,
    /// Paths to the rust files containing the zk computation.
    zk_compute_paths: Option<Vec<PathBuf>>,
    /// Path to pbc file.
    pbc_file: Option<PathBuf>,
    /// Path to wasm file.
    wasm_file: Option<PathBuf>,
    /// Path to abi file.
    abi_file: Option<PathBuf>,
    /// Path to zkbc file.
    zkbc_file: Option<PathBuf>,
}

impl ContractPathsBuilder {
    /// Initializes a builder with a package, target folder and flags.
    ///
    /// ### Parameters:
    ///
    /// * `package`: [`Vec<&Package>`] The package to build.
    ///
    /// * `target_folder`: [´&Path´] Path to the target folder.
    ///
    /// * `release`: [´bool´] If true, the contract is built with release and placed accordingly.
    ///
    /// * `zk`: [´bool´] If false, does not compile the zk part of the contracts.
    ///
    /// ### Returns:
    ///
    /// A [`ContractPathsBuilder`] with paths to pbc, wasm, abi and zkbc files.
    pub fn new(
        package: Package,
        target_folder: &Path,
        release: bool,
        zk: bool,
    ) -> ContractPathsBuilder {
        ContractPathsBuilder {
            package,
            target_folder: target_folder.to_path_buf(),
            release,
            zk,
            zk_compute_paths: None,
            pbc_file: None,
            wasm_file: None,
            abi_file: None,
            zkbc_file: None,
        }
    }

    /// Determines paths to pbc, wasm, abi and zkbc files and sets the field values.
    ///
    /// ### Returns:
    ///
    /// A [`ContractPathsBuilder`] with paths to pbc, wasm, abi and zkbc files.
    pub fn set_files(mut self) -> ContractPathsBuilder {
        self.pbc_file = Option::from(self.file_in_target_by_extension("pbc"));
        self.wasm_file = Option::from(self.file_in_target_by_extension("wasm"));
        self.abi_file = Option::from(self.file_in_target_by_extension("abi"));
        self.zkbc_file = Option::from(self.file_in_target_by_extension("zkbc"));

        self
    }

    /// Determines and sets paths to the zk computation files, if they exist.
    ///
    /// ### Returns:
    ///
    /// A [`ContractPathsBuilder`] with paths to zk computation files.
    pub fn set_zk_compute_paths(mut self) -> Result<ContractPathsBuilder, Box<dyn Error>> {
        let zk_compute_paths = if self.zk {
            get_zk_compute_paths(&self.package)?
        } else {
            None
        };
        self.zk_compute_paths = zk_compute_paths;

        Ok(self)
    }

    /// Builds an instance of a `ContractPaths` with the fields that have been set.
    ///
    /// ### Returns:
    ///
    /// A [`ContractPaths`]
    pub fn build(self) -> ContractPaths {
        ContractPaths {
            package: self.package,
            target_folder: self.target_folder,
            zk_compute_paths: self.zk_compute_paths,
            pbc_file: self.pbc_file.unwrap(),
            wasm_file: self.wasm_file.unwrap(),
            abi_file: self.abi_file.unwrap(),
            zkbc_file: self.zkbc_file.unwrap(),
        }
    }

    fn file_in_target_by_extension(&self, extension: &str) -> PathBuf {
        file_in_target(
            &self.package.name,
            self.release,
            extension,
            &self.target_folder,
        )
    }
}

/// Contract with paths to files and target folder.
#[derive(Clone)]
pub struct ContractPaths {
    /// The contract being built.
    pub(crate) package: Package,
    /// The path to the target folder.
    pub(crate) target_folder: PathBuf,
    /// An Option<Vec<String>> representing the paths to the zk computation files. None if not defined, or if the array was empty.
    pub(crate) zk_compute_paths: Option<Vec<PathBuf>>,
    /// Path to pbc file.
    pub(crate) pbc_file: PathBuf,
    /// Path to wasm file.
    pub(crate) wasm_file: PathBuf,
    /// Path to abi file.
    pub(crate) abi_file: PathBuf,
    /// Path to zkbc file.
    pub(crate) zkbc_file: PathBuf,
}
