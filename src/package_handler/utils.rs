use crate::build_command::build_package_error::BuildPackageError;
use crate::package_handler::state_visitor::StateVisitor;
use cargo_metadata::Metadata;
use cargo_metadata::Package;
use log::error;
use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::path::{Path, PathBuf};
use std::{fs, io};
use syn::visit::Visit;

/// Extract a list of contracts as a list of packages. A package is considered a contract if it
/// includes the state macro.
///
/// ### Parameters:
///
/// * `packages`: [`Vec<&Package>`], the metadata of the project.
///
/// ### Returns
///
/// The list of package id's that are considered contracts.
pub(crate) fn get_contracts(packages: Vec<&Package>) -> Result<Vec<Package>, Box<dyn Error>> {
    let mut contracts: Vec<Package> = vec![];
    for package in packages {
        // First grep for "#[state]" in all the files
        let mut src_path = PathBuf::from(package.manifest_path.clone());
        src_path.pop();
        let files = visit_dirs(&src_path, "rs").expect("Failed to find files in src folder");
        let files_with_state = grep_for_state(files);
        // If grep found something parse the file and check that the state macro is used
        for file_path in files_with_state {
            let path_string = file_path.to_str().unwrap();
            let mut file = File::open(file_path.clone())?;
            let mut src = String::new();
            file.read_to_string(&mut src)?;
            let syntax = syn::parse_file(&src).map_err(|_| -> BuildPackageError {
                error!("Unable to parse file: {path_string}. Try running 'cargo check'.");
                BuildPackageError
            })?;
            let mut visitor: StateVisitor = StateVisitor { state_found: false };
            visitor.visit_file(&syntax);
            if visitor.state_found {
                contracts.push(package.clone());
            }
        }
    }
    Ok(contracts)
}

/// Search through each file for the string "#\[state\]". If the file does contain this string
/// then the file is included in the returned list.
///
/// ### Parameters:
///
/// * `files`: [`Vec<PathBuf>`], the list of files to search through
///
/// ### Returns
/// A [`Vec<PathBuf>`] containing the paths to the files that contains the string "#\[state\]".
fn grep_for_state(files: Vec<PathBuf>) -> Vec<PathBuf> {
    let mut files_with_state: Vec<PathBuf> = vec![];
    for file_path in files {
        let file = File::open(file_path.clone()).expect("Failed to open file");
        let lines = BufReader::new(file).lines();
        for line in lines.map_while(Result::ok) {
            if line.contains("#[state]") {
                files_with_state.push(file_path.clone());
            }
        }
    }
    files_with_state
}

/// Walk through a directory only visiting files with the given extension.
///
/// ### Parameters:
///
/// * `dir`: &[`Path`], the dir to be visited.
/// * `extension`: [`&str`], the extension to search for.
///
/// ### Returns
///
/// A result containing paths to all the files in the directory.
fn visit_dirs(dir: &Path, extension: &str) -> io::Result<Vec<PathBuf>> {
    let mut files: Vec<PathBuf> = vec![];
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                let children = visit_dirs(&path, extension)?;
                files.extend(children);
            } else if path
                .extension()
                .and_then(|os_str| os_str.to_str())
                .eq(&Some(extension))
            {
                files.push(path);
            }
        }
    }
    Ok(files)
}

/// Get target folder using cargo metadata.
///
/// ### Parameters:
///
/// * `metadata`: &[`Metadata`], cargo metadata for the project.
///
/// ### Returns:
///
/// A [`PathBuf`] with the location of the target folder
pub(crate) fn get_target_folder_metadata(metadata: &Metadata) -> PathBuf {
    let target = metadata.target_directory.clone();
    let mut result = PathBuf::new();
    result.push(target);
    result
}

/// Get metadata for the current crate.
///
/// ### Parameters:
///
/// * `manifest_path`: &[`Option<String>`], path to the Cargo.toml
///
/// ### Returns:
/// [`Metadata`] for the crate.
pub(crate) fn get_metadata(
    manifest_path: &Option<String>,
) -> Result<Metadata, cargo_metadata::Error> {
    let mut cmd = cargo_metadata::MetadataCommand::new();
    if let Some(manifest) = manifest_path {
        cmd.manifest_path(manifest);
    }

    cmd.exec().inspect_err(|e| {
        error!("Problem with crate, got error: \n {e}. \n Try running 'cargo check'");
    })
}

/// Helper method for getting all the relevant packages in the workspace.
/// If called from a workspace member with `workspace` false only the single package is returned.
///
/// ### Parameters:
///
/// * `metadata`: &[`Metadata`], cargo metadata for the project.
///
/// * `workspace`: [`bool`], if true gets all workspace members in the workspace.
pub(crate) fn get_packages(
    metadata: &Metadata,
    workspace: bool,
    packages: Vec<String>,
) -> Result<Vec<&Package>, Box<dyn Error>> {
    let root_package = metadata.root_package();
    if workspace {
        Ok(metadata.workspace_packages())
    } else if !packages.is_empty() {
        match_patterns(metadata, packages)
    } else if root_package.is_none() {
        Ok(metadata.workspace_packages())
    } else {
        Ok(vec![root_package.unwrap()])
    }
}

/// Get the packages that matches any of the glob patterns provided.
/// Returns error if any of the patterns matches zero packages.
fn match_patterns(
    metadata: &Metadata,
    packages: Vec<String>,
) -> Result<Vec<&Package>, Box<dyn Error>> {
    let mut result = vec![];
    for pattern_str in packages {
        let pattern = glob::Pattern::new(&pattern_str).unwrap();
        let found: Vec<&Package> = metadata
            .workspace_packages()
            .into_iter()
            .filter(|p| pattern.matches(&p.name))
            .collect();
        if found.is_empty() {
            error!("Unable to find any packages matching '{}'", pattern_str);
            return Err(BuildPackageError.into());
        }
        result.extend(found);
    }
    result.sort_by(|p1, p2| p1.name.cmp(&p2.name));
    result.dedup();
    Ok(result)
}

/// Helper method for getting a PathBuf in for the target/wasm32-unknown-unknown folder.
/// The path is in /release/package_name.extension or /debug/package_name.extension in accordance
/// to the given parameters.
///
/// ### Parameters:
///
/// * `package_name`: [`&str`], the name of the package.
///
/// * `release`: [`bool`], whether to return a PathBuf for release or debug.
///
/// * `extension`: [`&str`], the extension used for the PathBuf.
///
/// * `target_folder`: &[`Path`], path to the target folder.
///
/// ### Returns:
///
/// The path in the target folder of type [`PathBuf`].
pub fn file_in_target(
    package_name: &str,
    release: bool,
    extension: &str,
    target_folder: &Path,
) -> PathBuf {
    let mut cwd = PathBuf::from(target_folder);
    cwd.push("wasm32-unknown-unknown");
    if release {
        cwd.push("release");
    } else {
        cwd.push("debug");
    }
    cwd.push(format!("{}.{}", package_name.replace('-', "_"), extension));

    cwd
}

/// Helper method to get the root package.
///
/// ### Parameters:
///
/// * `metadata`: &[`Metadata`], cargo metadata for the project.
///
/// ### Returns:
///
/// A [`Package`] representing the root package.
pub(crate) fn get_root_package(metadata: &Metadata) -> Package {
    metadata
        .root_package()
        .expect("No root package found in metadata")
        .clone()
}

#[cfg(test)]
mod tests {
    use crate::package_handler::utils::visit_dirs;
    use std::path::Path;

    #[test]
    fn visit_dirs_file() {
        let paths = visit_dirs(Path::new("cargo.toml"), "toml").unwrap();
        assert_eq!(paths.len(), 0);
    }
}
