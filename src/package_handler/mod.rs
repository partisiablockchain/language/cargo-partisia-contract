//! Handle packages and contracts. Provides auxiliary functions.
pub(crate) mod contract_paths;
/// State visitor for running over a syn ast for a rust file. Detects whether the file includes the state macro.
mod state_visitor;
pub(crate) mod utils;
