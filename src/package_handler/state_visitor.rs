use syn::__private::Span;
use syn::visit::Visit;
use syn::{visit, Ident, ItemStruct};

/// Struct for the StateVisitor. Able to run over a syn::File and check whether the state macro
/// is in the file.
///
/// ### Fields:
///
/// * `state_found`: [`bool`], bool for finding out whether the state macro is used on a struct in the file.
pub(crate) struct StateVisitor {
    /// The state_found bool.
    pub(crate) state_found: bool,
}

/// Implementation for syntax tree traversal for StateVisitor. Overrides the normal visit method to additionally
/// update the state_found field if the syntax tree uses the state macro on a struct definition.
impl<'ast> Visit<'ast> for StateVisitor {
    fn visit_item_struct(&mut self, node: &'ast ItemStruct) {
        visit::visit_item_struct(self, node);
        for attribute in node.attrs.clone() {
            for segment in &attribute.path().segments {
                if segment.ident == Ident::new("state", Span::call_site()) {
                    self.state_found = true;
                    return;
                }
            }
        }
    }
}
