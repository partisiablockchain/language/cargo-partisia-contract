use std::error::Error;
use std::fmt;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug)]
pub(crate) struct BuildPackageError;

impl Display for BuildPackageError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "Build failed, see console output for more details.")
    }
}

impl Error for BuildPackageError {}
