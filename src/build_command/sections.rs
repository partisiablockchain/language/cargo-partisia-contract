use crate::build_command::section_identifiers::{
    ABI_IDENTIFIER_BYTE, ORDERED_IDENTIFIERS, WASM_IDENTIFIER_BYTE, ZK_IDENTIFIER_BYTE,
};
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::path::Path;

/// The sections of a sectioned file.
/// The section format can be found [here](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#sections).
/// Can be used for creating sectioned files, i.e. a .pbc file.
/// A section consists of an identifier byte, a 32-bit unsigned integer indicating the length
/// of the section and lastly the content bytes of the section.
/// ### Fields
/// * `sections`: [`HashMap<u8, Vec<u8>>`], a mapping from identifier bytes of sections to their
///     content bytes.
pub(crate) struct Sections {
    sections: HashMap<u8, Vec<u8>>,
}

impl Sections {
    /// Adds section bytes and a corresponding identifier byte to the collection of sections.
    ///
    /// ### Parameters:
    ///
    /// * `identifier`: ['u8'], header byte indicating the type of the following section in the pbc file.
    ///
    /// * `section`: [`Vec<u8>`], the bytes of the section to be added.
    pub(crate) fn add_section(&mut self, identifier: u8, file: &Path) {
        self.sections
            .insert(identifier, Sections::read_bytes_from_file(file));
    }

    /// Returns the serialized bytes of the sections of a [".pbc" file](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format).
    /// A .pbc file begins with the magic header string "PBSC".
    /// It then contains an ABI section, a WASM section and if present a ZKBC section in that order.
    /// Requires that an ABI section and a WASM section have been added.
    /// ### Returns
    /// the serialized PBC sections
    /// ### Panics
    /// if either an ABI section or a WASM section has not been added.
    pub(crate) fn get_pbc_bytes(&self) -> Vec<u8> {
        assert!(
            self.sections.contains_key(&ABI_IDENTIFIER_BYTE),
            "Cannot build .pbc file: No ABI section provided"
        );
        assert!(
            self.sections.contains_key(&WASM_IDENTIFIER_BYTE),
            "Cannot build .pbc file: No WASM section provided"
        );
        let mut section_bytes: Vec<u8> = vec![];
        for identifier in ORDERED_IDENTIFIERS {
            if self.sections.contains_key(&identifier) {
                // add section identifier
                section_bytes.push(identifier);

                // read bytes from file and parse length
                let section_len: [u8; 4] =
                    u32::to_be_bytes(self.sections[&identifier].len() as u32);

                // add bytes to section
                section_bytes.extend_from_slice(&section_len);
                section_bytes.append(&mut self.sections[&identifier].clone());
            }
        }
        section_bytes
    }

    /// Returns the serialized bytes of the sections contained in the ZKWA format.
    /// First WASM, then ZKBC. Requires that both a WASM section and a ZKBC section have been added.
    /// ### Returns
    /// the serialized ZKWA sections
    /// ### Panics
    /// if either a WASM section or a ZKBC section has not been added.
    pub(crate) fn get_zkwa_bytes(&self) -> Vec<u8> {
        assert!(
            self.sections.contains_key(&WASM_IDENTIFIER_BYTE),
            "Cannot build .zkwa file: No WASM section provided"
        );
        assert!(
            self.sections.contains_key(&ZK_IDENTIFIER_BYTE),
            "Cannot build .zkwa file: No ZKBC section provided"
        );

        let mut section_bytes: Vec<u8> = vec![];
        // add section identifier
        section_bytes.push(WASM_IDENTIFIER_BYTE);

        // read bytes from file and parse length
        let section_len: [u8; 4] =
            u32::to_be_bytes(self.sections[&WASM_IDENTIFIER_BYTE].len() as u32);

        // add bytes to section
        section_bytes.extend_from_slice(&section_len);
        section_bytes.append(&mut self.sections[&WASM_IDENTIFIER_BYTE].clone());

        // add section identifier
        section_bytes.push(ZK_IDENTIFIER_BYTE);

        // read bytes from file and parse length
        let section_len: [u8; 4] =
            u32::to_be_bytes(self.sections[&ZK_IDENTIFIER_BYTE].len() as u32);

        // add bytes to section
        section_bytes.extend_from_slice(&section_len);
        section_bytes.append(&mut self.sections[&ZK_IDENTIFIER_BYTE].clone());
        section_bytes
    }

    /// Creates an empty collection of sections.
    pub(crate) fn new() -> Self {
        Sections {
            sections: HashMap::new(),
        }
    }

    /// Reads and returns the bytes from a file.
    /// ### returns
    /// the read bytes.
    /// ### Panics
    /// if the file doesn't exist.
    fn read_bytes_from_file(file: &Path) -> Vec<u8> {
        let mut bytes: Vec<u8> = Vec::new();
        let mut file_pointer: File = File::open(file).unwrap();
        file_pointer.read_to_end(&mut bytes).unwrap();
        bytes
    }
}
