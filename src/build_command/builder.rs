use crate::build_command::build_package::BuildCommand;
use crate::build_command::build_package_error::BuildPackageError;
use crate::package_handler::contract_paths::ContractPathsBuilder;
use crate::package_handler::utils::{
    get_contracts, get_metadata, get_packages, get_target_folder_metadata,
};
use log::error;
use std::error::Error;

#[derive(Default)]
/// Builder for the CommandBuilder
pub struct BuildCommandBuilder {
    /// `release`: [bool], If true, the contract is built with release and placed accordingly.
    pub(crate) release: bool,
    /// `no_abi`: [bool], If false, generates the abi.
    pub(crate) no_abi: bool,
    /// `quiet`: [bool], If true, no messages are printed to stdout.
    pub(crate) quiet: bool,
    /// `no_wasm_strip`: [bool], If false, strips the generated wasm file.
    pub(crate) no_wasm_strip: bool,
    /// `no_zk`: [bool], If true, does not compile the zk part of the contracts.
    pub(crate) no_zk: bool,
    /// `disable_git_fetch_with_cli`: [bool], If true, uses cargo's built-in git library to fetch dependencies instead of the git executable.
    pub(crate) disable_git_fetch_with_cli: bool,
    /// `manifest_path`: [Option<String>], Path to the Cargo.toml of the contract or workspace
    pub(crate) manifest_path: Option<String>,
    /// `workspace`: [bool], If true, build all packages in the workspace
    pub(crate) workspace: bool,
    /// `coverage`: [bool], If true, enables generation of coverage files
    pub(crate) coverage: bool,
    /// `packages`: [Vec<String>], packages to build
    pub(crate) packages: Vec<String>,
    /// `additional_args`: [Vec<String>], Additional arguments
    pub(crate) additional_args: Vec<String>,
}

impl BuildCommandBuilder {
    #[must_use]
    pub fn builder() -> BuildCommandBuilder {
        BuildCommandBuilder::new()
    }

    pub fn new() -> BuildCommandBuilder {
        BuildCommandBuilder {
            release: false,
            no_abi: false,
            quiet: false,
            no_wasm_strip: false,
            no_zk: true,
            disable_git_fetch_with_cli: false,
            manifest_path: None,
            workspace: false,
            coverage: false,
            packages: vec![],
            additional_args: vec![],
        }
    }

    pub fn set_release(mut self, release: bool) -> BuildCommandBuilder {
        self.release = release;
        self
    }

    pub fn set_no_abi(mut self, no_abi: bool) -> BuildCommandBuilder {
        self.no_abi = no_abi;
        self
    }

    pub fn set_quiet(mut self, quiet: bool) -> BuildCommandBuilder {
        self.quiet = quiet;
        self
    }

    pub fn set_no_wasm_strip(mut self, no_wasm_strip: bool) -> BuildCommandBuilder {
        self.no_wasm_strip = no_wasm_strip;
        self
    }

    pub fn set_no_zk(mut self, no_zk: bool) -> BuildCommandBuilder {
        self.no_zk = no_zk;
        self
    }

    pub fn set_disable_git_fetch_with_cli(
        mut self,
        disable_git_fetch_with_cli: bool,
    ) -> BuildCommandBuilder {
        self.disable_git_fetch_with_cli = disable_git_fetch_with_cli;
        self
    }
    pub fn set_workspace(mut self, workspace: bool) -> BuildCommandBuilder {
        self.workspace = workspace;
        self
    }
    pub fn set_coverage(mut self, coverage: bool) -> BuildCommandBuilder {
        self.coverage = coverage;
        self
    }

    pub fn set_packages(mut self, packages: Vec<String>) -> BuildCommandBuilder {
        if !packages.is_empty() {
            self.packages = packages;
        }
        self
    }

    pub fn set_additional_args(mut self, additional_args: Vec<String>) -> BuildCommandBuilder {
        if !additional_args.is_empty() {
            self.additional_args = additional_args;
        }
        self
    }

    pub fn set_manifest_path(mut self, manifest_path: Option<String>) -> BuildCommandBuilder {
        self.manifest_path = manifest_path;
        self
    }

    pub fn build_command(self) -> Result<BuildCommand, Box<dyn Error>> {
        let metadata = get_metadata(&self.manifest_path)?;
        let packages = get_packages(&metadata, self.workspace, self.packages)?;

        if self.coverage && self.no_abi {
            error!("Cannot run coverage without generating an abi file");
            return Err(BuildPackageError.into());
        }

        let target_folder = &get_target_folder_metadata(&metadata);
        let mut contract_paths = vec![];
        let contracts = get_contracts(packages.clone())?;
        for package in contracts.clone() {
            let contract_paths_builder =
                ContractPathsBuilder::new(package, target_folder, self.release, !self.no_zk)
                    .set_files()
                    .set_zk_compute_paths()?;
            let contract = contract_paths_builder.build();
            contract_paths.push(contract);
        }

        let non_contract_packages = packages.len() > contracts.len();

        let build_command = BuildCommand {
            release: self.release,
            abi: !self.no_abi,
            quiet: self.quiet,
            wasm_strip: !self.no_wasm_strip,
            disable_git_fetch_with_cli: self.disable_git_fetch_with_cli,
            manifest_path: self.manifest_path,
            workspace: self.workspace,
            coverage: self.coverage,
            additional_args: self.additional_args,
            contracts: contract_paths,
            metadata,
            non_contracts: non_contract_packages,
        };
        Ok(build_command)
    }
}
