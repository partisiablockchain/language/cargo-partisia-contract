//! Building Rust contracts and non-contracts.
//!
//! The`BuildCommand` struct which takes the command arguments and metadata as its parameters.
//! Contracts are built and compiled on Partisia Blockchain.
//!
//! Types of contracts include zk contracts and non-zk contracts.
//!
//! The file created for a zk contract is the zkwa, and wasm for a non-zk-contract.
//! These files can also be combined with the abi into a single pbc file.
//!
use cargo_metadata::{Metadata, Package};
use std::error::Error;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

use log::{debug, error, info};

use crate::build_command::build_package_error::BuildPackageError;
use crate::build_command::contract_coverage::generate_coverage;
use crate::build_command::section_identifiers::{
    ABI_IDENTIFIER_BYTE, PBC_HEADER_BYTES, WASM_IDENTIFIER_BYTE, ZK_IDENTIFIER_BYTE,
};
use crate::build_command::sections::Sections;
use crate::constants::ABI_CLI;
use crate::get_compiler::{get_fetch_metadata, retrieve_jar, retrieve_zk_compiler, FetchMetadata};
use crate::jar_command::{java_jar_command, CallableJar};
use crate::package_handler::contract_paths::ContractPaths;
use crate::package_handler::utils::file_in_target;
use crate::wasm_gc;

/// Executor of the build command, used for building contracts and non-contracts.
pub struct BuildCommand {
    /// `release`: [bool], If true, the contract is built with release and placed accordingly.
    pub(crate) release: bool,
    /// `abi`: [bool], If true, generates the abi.
    pub(crate) abi: bool,
    /// `quiet`: [bool], If true, no messages are printed to stdout.
    pub(crate) quiet: bool,
    /// `wasm_strip`: [bool], If true, strips the generated wasm file.
    pub(crate) wasm_strip: bool,
    /// `disable_git_fetch_with_cli`: [bool], If true, uses cargo's built-in git library to fetch dependencies instead of the git executable.
    pub(crate) disable_git_fetch_with_cli: bool,
    /// `manifest_path`: [Option<String>], Path to the Cargo.toml of the contract or workspace
    pub(crate) manifest_path: Option<String>,
    /// `workspace`: [bool], If true, build all packages in the workspace
    pub(crate) workspace: bool,
    /// `coverage`: [bool], If true, enables generation of coverage files
    pub(crate) coverage: bool,
    /// `additional_args`: [Vec<String>], Additional arguments that will be passed along to cargo build.
    pub(crate) additional_args: Vec<String>,
    ///  `contracts`: [Vec<ContractPaths>] Contains contracts and paths to relevant files.
    pub(crate) contracts: Vec<ContractPaths>,
    /// `metadata`: [Metadata] Cargo metadata for the project.
    pub(crate) metadata: Metadata,
    /// `non_contracts`: [bool] If true, the workspace contains non-contract packages.
    pub(crate) non_contracts: bool,
}

impl BuildCommand {
    /// Build all contracts and non-contracts.
    ///
    /// ### Returns:
    ///
    /// An empty result or a heap allocated Error.
    pub fn start_build(&self) -> Result<(), Box<dyn Error>> {
        let installed_targets = get_installed_targets()?;
        check_wasm32_unknown_unknown(installed_targets)?;

        self.build_contracts()?;

        if self.non_contracts {
            self.build_non_contracts()?;
        }
        if self.coverage {
            generate_coverage(
                &self.contracts,
                self.release,
                &self.metadata.workspace_metadata,
            )?;
        }
        Ok(())
    }

    /// Build all contracts one by one.
    ///
    /// ### Returns:
    ///
    /// An empty result or a heap allocated Error.
    fn build_contracts(&self) -> Result<(), Box<dyn Error>> {
        for contract_paths in &self.contracts {
            let mut sections = Sections::new();

            self.display_contract_info(self.abi, contract_paths);

            if self.abi {
                sections = self.create_abi(contract_paths, sections)?;
            }

            self.build_contract(contract_paths, false)?;
            sections.add_section(WASM_IDENTIFIER_BYTE, &contract_paths.wasm_file);

            if let Some(zk_compute_paths) = &contract_paths.zk_compute_paths {
                sections = self.compile_zkbc(contract_paths, zk_compute_paths, sections)?;
                sections = self.create_zkwa(contract_paths, sections);
            }

            self.create_pbc(&contract_paths.pbc_file.to_path_buf(), sections);
        }
        Ok(())
    }

    /// Adds information about the contract; ABI, release and zk flags.
    ///
    /// ### Parameters:
    ///
    /// * `abi`: &[`bool`], Whether ABI is be generated.
    ///
    /// * `package`: &[`Package`], The package that is being built.
    ///
    /// * `contract_paths`: &[`ContractPaths`], the contract being built and paths to its associated files.
    ///
    fn display_contract_info(&self, abi: bool, contract_paths: &ContractPaths) {
        info!(
            "Building contract {} (ABI: {}) (release: {})  (zk: {})",
            contract_paths.package.name.clone(),
            abi,
            self.release,
            contract_paths.zk_compute_paths.is_some()
        );
        info!("Cargo output:");
    }

    /// Creates .pbc file at the specified location.
    ///
    /// ### Parameters:
    ///
    /// * `pbc_file`: &[`PathBuf`], Path to the .pbc file.
    ///
    /// * `sections`: &[`Sections`], The sections of the sectioned file.
    ///
    fn create_pbc(&self, pbc_file: &PathBuf, sections: Sections) {
        info!("Creating .pbc file at: {}", pbc_file.display());
        let mut section_bytes: Vec<u8> = Vec::from(PBC_HEADER_BYTES);
        section_bytes.append(&mut sections.get_pbc_bytes());
        fs::write(pbc_file, &section_bytes).unwrap();
    }

    /// Creates .zkwa file at the specified location.
    ///
    /// ### Parameters:
    ///
    /// * `contract_paths`: &[`ContractPaths`], the contract being built and paths to its associated files.
    ///
    /// * `sections`: &[`Sections`], The sections of the sectioned file.
    ///
    /// ### Returns:
    ///
    /// Sections.
    fn create_zkwa(&self, contract_paths: &ContractPaths, sections: Sections) -> Sections {
        let zkwa_file = file_in_target(
            contract_paths.package.name.as_str(),
            self.release,
            "zkwa",
            &contract_paths.target_folder,
        );
        info!(
            "Linking wasm and zkbc into zkwa file: {}",
            zkwa_file.display()
        );
        fs::write(&zkwa_file, sections.get_zkwa_bytes()).unwrap();

        sections
    }

    /// Compiles the zero knowledge computations.
    ///
    /// ### Parameters:
    ///
    /// * `contract_paths`: &[`ContractPaths`], the contract being built and paths to its associated files.
    ///
    /// * `zk_compute_paths`: &[`Vec<PathBuf>`], An Option<Vec<String>> representing
    ///      the paths to the zk computation files. None if not defined, or if the array was empty.
    ///
    /// * `sections`: &[`Sections`], The sections of the sectioned file.
    ///
    /// ### Returns:
    ///
    /// Sections or a heap allocated Error.
    fn compile_zkbc(
        &self,
        contract_paths: &ContractPaths,
        zk_compute_paths: &Vec<PathBuf>,
        mut sections: Sections,
    ) -> Result<Sections, Box<dyn Error>> {
        let zk_compute_paths_display: Vec<String> = zk_compute_paths
            .to_owned()
            .iter()
            .map(|path| path.display().to_string())
            .collect::<Vec<String>>();
        info!(
            "Using zk-compute-paths: [{}]",
            zk_compute_paths_display.join(" ")
        );

        for zk_compute in contract_paths.zk_compute_paths.clone().unwrap() {
            if !&zk_compute.exists() {
                error!(
                    "zk computation file does not exist: {}",
                    zk_compute.display()
                );
                return Err(BuildPackageError.into());
            }
        }

        let zk_compiler_file: PathBuf =
            retrieve_zk_compiler(true, &contract_paths.package, &self.metadata)?;
        info!(
            "Compiling zero knowledge computation using {}",
            zk_compiler_file.display()
        );

        self.compile_zk_computation(
            contract_paths.zkbc_file.clone(),
            contract_paths.zk_compute_paths.clone().unwrap().to_owned(),
            zk_compiler_file,
        )?;
        info!(
            "Compiled zero knowledge computation: {}",
            contract_paths.zkbc_file.to_str().unwrap()
        );

        sections.add_section(ZK_IDENTIFIER_BYTE, &contract_paths.zkbc_file);

        Ok(sections)
    }

    /// Builds the public part of the contract as well as generating the abi.
    ///
    /// ### Parameters:
    ///
    /// * `contract_paths`: &[`ContractPaths`], the contract being built and paths to its associated files.
    ///
    /// ### Returns:
    ///
    /// Sections or a heap allocated Error.
    fn create_abi(
        &self,
        contract_paths: &ContractPaths,
        mut sections: Sections,
    ) -> Result<Sections, Box<dyn Error>> {
        self.build_contract(contract_paths, true)?;
        info!(
            "Built contract: {}\n",
            contract_paths.wasm_file.to_str().unwrap()
        );

        info!("Creating ABI");
        self.create_abi_from_wasm(
            &contract_paths.wasm_file,
            &contract_paths.abi_file,
            &contract_paths.package,
        )?;

        info!(
            "ABI created: {}\n",
            contract_paths.abi_file.to_str().unwrap()
        );

        sections.add_section(ABI_IDENTIFIER_BYTE, &contract_paths.abi_file);

        Ok(sections)
    }

    /// Build all packages that is not contracts together using cargo build.
    ///
    /// ### Returns:
    ///
    /// An empty result or a heap allocated Error.
    fn build_non_contracts(&self) -> Result<(), Box<dyn Error>> {
        info!("Building non-contract packages");
        let mut process = Command::new("cargo");
        process
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .arg("build");
        if let Some(manifest) = self.manifest_path.clone() {
            process.arg("--manifest-path").arg(manifest);
        }
        if self.workspace || !self.contracts.is_empty() {
            process.arg("--workspace");
        }
        if self.quiet {
            process.arg("--quiet");
        }
        for contract in self.contracts.clone() {
            process.arg("--exclude").arg(&contract.package.name);
        }
        process.args(&self.additional_args);

        let output = process.output()?;

        if let Some(0) = output.status.code() {
            Ok(())
        } else {
            Err(BuildPackageError.into())
        }
    }

    /// Builds the public part of the contract as well as generating the abi if `generate_abi` is true.
    ///
    /// ### Parameters:
    ///
    /// * `contract_paths`: &[`ContractPaths`], the contract being built and paths to its associated files.
    ///
    /// * `generate_abi`: [`bool`], if true also generates the abi.
    ///
    /// ### Returns:
    ///
    /// An empty result or a heap allocated Error.
    ///
    fn build_contract(
        &self,
        contract_paths: &ContractPaths,
        generate_abi: bool,
    ) -> Result<(), Box<dyn Error>> {
        let mut process = Command::new("cargo");
        process
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .arg("build")
            .arg("--manifest-path")
            .arg(contract_paths.package.manifest_path.clone())
            .arg("-p")
            .arg(contract_paths.package.name.clone());
        if !self.disable_git_fetch_with_cli {
            process.env("CARGO_NET_GIT_FETCH_WITH_CLI", "true");
        }
        process.env("CARGO_INCREMENTAL", "1");

        if generate_abi {
            process.arg("--features").arg("abi");
        }

        process.arg("--target").arg("wasm32-unknown-unknown");

        if self.release {
            process.arg("--release");
        }

        if self.quiet {
            process.arg("--quiet");
        }

        process.args(&self.additional_args);

        let output = process.output()?;

        if let Some(0) = output.status.code() {
            if self.wasm_strip {
                self.wasm_strip(contract_paths)?;
            }
            Ok(())
        } else {
            Err(BuildPackageError.into())
        }
    }

    /// The abi client reads the wasm to generate the abi, and writes the abi to `bin_file`.
    ///
    /// ### Parameters:
    ///
    /// * `wasm_file`: &[`Path`], the path to the created wasm_file.
    ///
    /// * `bin_file`: &[`Path`], the location to place the created abi file.
    ///
    /// ### Returns:
    ///
    /// An empty result or a heap allocated Error.
    ///
    fn create_abi_from_wasm(
        &self,
        wasm_file: &Path,
        bin_file: &Path,
        package: &Package,
    ) -> Result<(), Box<dyn Error>> {
        info!("wasm_file: {}", wasm_file.display());

        let abi_cli_meta =
            get_fetch_metadata(&self.metadata.workspace_metadata, package, "abi-cli");
        let abi_cli = abi_cli_meta.map(|f| {
            let FetchMetadata::Http { url } = f;
            url
        });

        let abi_client = CallableJar::from(retrieve_jar(&abi_cli, ABI_CLI)?);
        abi_client.save_jar_fetch_config(ABI_CLI)?;
        let output = abi_client.call_jar(
            &vec![
                "from-wasm".to_string(),
                wasm_file.to_string_lossy().into(),
                bin_file.to_string_lossy().into(),
            ],
            None,
            None,
        )?;

        Ok(output).map(|_| ()) // is Ok(()) the same here?
    }

    /// Strips the generated wasm file found using the release flag.
    ///
    /// ### Parameters:
    ///
    /// * `package`: [`Package`], the package that was built.
    ///
    /// * `release`: [`bool`], used for finding the generated wasm file.
    ///
    /// * `metadata`: &[`Metadata`], cargo metadata for the project.
    ///
    fn wasm_strip(&self, package_util: &ContractPaths) -> Result<(), Box<dyn Error>> {
        if self.release {
            debug!("Removing custom sections of WASM file");
            wasm_gc::remove_custom_sections(&package_util.wasm_file.to_path_buf())?;
        }
        Ok(())
    }

    /// Build the secret part of the contract and link it together with the public part.
    ///
    /// ### Parameters:
    ///
    /// * `zkbc_file`: [`PathBuf`], path to the generated zk part of the contract.
    ///
    /// * `zk_compute_file`: [`Vec<PathBuf>`], paths to the rust files containing the zk computation.
    ///
    /// * `zk_compiler_file`: [`PathBuf`], path to the zk-compiler jar.
    ///
    fn compile_zk_computation(
        &self,
        zkbc_file: PathBuf,
        zk_compute_files: Vec<PathBuf>,
        zk_compiler_file: PathBuf,
    ) -> Result<(), Box<dyn Error>> {
        if !zk_compiler_file.exists() {
            error!(
                "Expected zk-compiler to be located at: {}",
                zk_compiler_file.to_str().unwrap()
            );
            return Err(BuildPackageError.into());
        }
        let mut process = java_jar_command()?;
        process.arg(zk_compiler_file.to_str().unwrap());
        for zk_compute_file in zk_compute_files {
            process.arg(zk_compute_file.to_str().unwrap());
        }
        process.arg("-o");
        process.arg(zkbc_file.to_str().unwrap());

        let output = process.output()?;

        if let Some(0) = output.status.code() {
            Ok(())
        } else {
            Err(BuildPackageError.into())
        }
    }
}

fn get_installed_targets() -> Result<String, Box<dyn Error>> {
    let mut process = Command::new("rustup");
    process.arg("target").arg("list").arg("--installed");
    let output = process.output()?;
    let string = String::from_utf8(output.stdout)?;
    Ok(string)
}

pub(crate) fn check_wasm32_unknown_unknown(string: String) -> Result<(), Box<dyn Error>> {
    if string.contains("wasm32-unknown-unknown") {
        Ok(())
    } else {
        error!(
            "Missing target 'wasm32-unknown-unknown'\n\
            Try installing it with 'rustup target add wasm32-unknown-unknown'"
        );
        Err(BuildPackageError.into())
    }
}
