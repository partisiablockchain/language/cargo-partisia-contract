/// The magic header "PBSC" in ASCII. The magic bytes are used in the start of a ".pbc" file. The format description for ".pbc" can be found here (https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format).
pub const PBC_HEADER_BYTES: [u8; 4] = [0x50, 0x42, 0x53, 0x43]; // ASCII values for PBSC
/// Identifier byte indicating the beginning of an ABI section of a ".pbc" file
pub const ABI_IDENTIFIER_BYTE: u8 = 0x1;
/// Identifier byte indicating the beginning of a WASM section of a ".pbc" file
pub const WASM_IDENTIFIER_BYTE: u8 = 0x2;
/// Identifier byte indicating the beginning of a ZKBC section of a ".pbc" file
pub const ZK_IDENTIFIER_BYTE: u8 = 0x3;
/// Identifiers given in the order corresponding to the [".pbc" format](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format)
pub const ORDERED_IDENTIFIERS: [u8; 3] = [
    ABI_IDENTIFIER_BYTE,
    WASM_IDENTIFIER_BYTE,
    ZK_IDENTIFIER_BYTE,
];
