use crate::build_command::build_package_error::BuildPackageError;
use crate::constants::ABI_CLI;
use crate::get_compiler::{get_fetch_metadata, retrieve_jar, FetchMetadata};
use crate::get_versions;
use crate::jar_command::CallableJar;
use crate::package_handler::contract_paths::ContractPaths;
use crate::package_handler::utils::file_in_target;
use cargo_metadata::Package;
use log::{error, info};
use serde_json::Value;
use std::error::Error;
use std::fs;
use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

/// Generate a native runner executable for each given contract with instrumentation enabled.
/// This executable enables generation of profraw files for getting a coverage report.
/// The executable will be located next to the .abi and .wasm files.
///
/// ### Parameters:
///
/// * `contracts`: &[`Vec<ContractPaths>`], the list of packages for which to generate an executable for.
///
/// * `target_folder`: &[`Path`], path to the target folder.
///
/// * `release`: [`bool`], whether build is run in release or debug mode.
pub(crate) fn generate_coverage(
    contracts: &Vec<ContractPaths>,
    release: bool,
    metadata: &Value,
) -> Result<(), Box<dyn Error>> {
    for contract_paths in contracts {
        let contract = &contract_paths.package;
        let target_folder = &contract_paths.target_folder;
        let abi_cli_url = get_abi_cli_url(metadata, contract);
        generate_native_runner(contract, target_folder, release, &abi_cli_url)?;
        generate_coverage_executable(contract, target_folder, release)?;
    }
    Ok(())
}

fn get_abi_cli_url(metadata: &Value, package: &Package) -> Option<String> {
    let abi_cli_meta = get_fetch_metadata(metadata, package, "abi-cli");
    abi_cli_meta.map(|f| {
        let FetchMetadata::Http { url } = f;
        url
    })
}

fn generate_native_runner(
    contract: &Package,
    target_folder: &Path,
    release: bool,
    abi_cli_url: &Option<String>,
) -> Result<(), Box<dyn Error>> {
    let actions = get_actions(contract, target_folder, release, abi_cli_url)?;
    let call_map = build_call_map(actions, contract.name.as_str());

    let wasm_file = file_in_target(contract.name.as_str(), release, "wasm", target_folder);
    let binder_version = get_versions(&wasm_file, abi_cli_url)?.binder;

    create_native_runner_main(
        call_map,
        binder_version,
        contract.name.as_str(),
        target_folder,
    )?;
    create_native_runner_toml(contract, target_folder)?;

    Ok(())
}

fn create_native_runner_toml(
    contract: &Package,
    target_folder: &Path,
) -> Result<(), Box<dyn Error>> {
    let toml_bytes: &[u8] = include_bytes!("../templates/native-contract-runner.toml.jinja2");

    let toml_string = String::from_utf8(toml_bytes.to_vec()).unwrap();
    let toml_template = liquid::ParserBuilder::with_stdlib()
        .build()
        .unwrap()
        .parse(&toml_string)
        .unwrap();
    let contract_name = contract.name.as_str();
    let mut package_path = contract.manifest_path.clone();
    package_path.pop();
    let package_path = package_path.to_string().replace('\\', "/");
    let pbc_lib_src = contract
        .dependencies
        .iter()
        .find(|dep| dep.name == "pbc_lib")
        .unwrap()
        .source
        .as_ref()
        .unwrap()
        .as_str();
    let pbc_lib_dep: Vec<&str> = pbc_lib_src.split(['+', '?', '=']).collect();
    let toml_globals = liquid::object!({
        "contract_dependency": contract_name,
        "package_path": package_path,
        "pbc_lib_git": pbc_lib_dep[1],
        "pbc_lib_tag": pbc_lib_dep[2],
        "pbc_lib_version": pbc_lib_dep[3],
    });

    let toml_string = toml_template.render(&toml_globals).unwrap();
    let package_location = PathBuf::from(target_folder)
        .join("coverage")
        .join(contract_name)
        .join("native-contract-runner");
    create_dir_all(&package_location)?;

    let mut buffer = File::create(package_location.join("Cargo.toml"))?;
    buffer.write_all(toml_string.as_bytes())?;
    Ok(())
}

fn create_native_runner_main(
    call_map: String,
    binder_version: [u8; 3],
    contract_name: &str,
    target_folder: &Path,
) -> Result<(), Box<dyn Error>> {
    let main_bytes: &[u8] = include_bytes!("../templates/native-contract-runner.rs.jinja2");

    let main_string = String::from_utf8(main_bytes.to_vec()).unwrap();
    let main_template = liquid::ParserBuilder::with_stdlib()
        .build()
        .unwrap()
        .parse(&main_string)
        .unwrap();

    let main_globals = liquid::object!({
        "CALL_MAP": call_map,
        "BINDER_VERSION": format!("{:?}", binder_version),
    });

    let main_string = main_template.render(&main_globals).unwrap();
    let src_location = PathBuf::from(target_folder)
        .join("coverage")
        .join(contract_name)
        .join("native-contract-runner")
        .join("src");
    create_dir_all(&src_location)?;

    let mut buffer = File::create(src_location.join("main.rs"))?;
    buffer.write_all(main_string.as_bytes())?;
    Ok(())
}

/// Creates rust code initializing a [`std::collections::HashMap`] from action function names to the
/// action's wrapped function pointer.
fn build_call_map(mut actions: Vec<(String, String)>, package_name: &str) -> String {
    let contract_name = package_name.replace('-', "_");

    let mut str_accum: Vec<String> = vec![];
    str_accum.push("[\n".to_owned());
    actions.sort_by_key(|(k, _v)| k.clone());
    for (function_name, action) in actions {
        str_accum.push(format!(
            "\t\t(\"{function_name}\", {contract_name}::__pbc_autogen__{action}_wrapped as extern \"C-unwind\" fn(*mut u8, usize) -> u64),\n"
        ));
    }
    str_accum.push("]".to_owned());
    str_accum.join("")
}

fn get_actions(
    contract: &Package,
    target_folder: &Path,
    release: bool,
    abi_cli_url: &Option<String>,
) -> Result<Vec<(String, String)>, Box<dyn Error>> {
    let path_to_abi = file_in_target(contract.name.as_str(), release, "abi", target_folder);
    let path_to_abi_str = format!("{}", path_to_abi.to_string_lossy());
    let abi_args = vec!["list-of-actions".to_string(), path_to_abi_str];

    let abi_client = CallableJar::from(retrieve_jar(abi_cli_url, ABI_CLI)?);
    let output = abi_client.call_jar(&abi_args, Some(Stdio::piped()), None)?;
    let actions = format!("{}", String::from_utf8_lossy(&output.stdout));
    let actions_vec: Vec<(String, String)> = actions
        .split('\n')
        .filter(|x| !x.is_empty() && x.contains(':'))
        .map(|action| {
            let shortname_name: Vec<&str> = action.split(':').collect();
            (shortname_name[0].to_string(), shortname_name[1].to_string())
        })
        .collect();
    Ok(actions_vec)
}

fn generate_coverage_executable(
    contract: &Package,
    target_folder: &Path,
    release: bool,
) -> Result<(), Box<dyn Error>> {
    let workdir = target_folder
        .join("coverage")
        .join(&contract.name)
        .join("native-contract-runner");
    info!("Building native-contract-runner for {}", contract.name);
    let mut process = Command::new("cargo");
    process
        .stdout(Stdio::null())
        .stderr(Stdio::inherit())
        .arg("build");
    process.env("RUSTFLAGS", "-C instrument-coverage");

    let coverage_format = target_folder
        .join("coverage")
        .join("profraw")
        .join(format!("cov-{}-%p.profraw", contract.name));

    process.env("LLVM_PROFILE_FILE", coverage_format);

    let manifest = workdir.join("Cargo.toml");
    process.arg("--manifest-path").arg(manifest);
    process.arg("--quiet");

    let output = process.output()?;

    if Some(0) != output.status.code() {
        error!(
            "Failed to compile native-contract-runner for {}",
            contract.name
        );
        return Err(BuildPackageError.into());
    }
    let executable_file_name = contract.name.to_string() + "_runner" + std::env::consts::EXE_SUFFIX;
    let expected_out = workdir
        .join("target")
        .join("debug")
        .join(&executable_file_name);
    let mut new_out = target_folder.join("wasm32-unknown-unknown");
    if release {
        new_out.push("release");
    } else {
        new_out.push("debug");
    }
    let new_file_name = executable_file_name.replace('-', "_");
    new_out.push(new_file_name);
    fs::rename(expected_out, new_out)?;
    Ok(())
}
