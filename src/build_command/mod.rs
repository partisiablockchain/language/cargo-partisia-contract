//! Build Rust contracts and non-contracts.
pub(crate) mod build_package;
pub(crate) mod build_package_error;
pub(crate) mod builder;
pub(crate) mod contract_coverage;
/// The magic header "PBSC" in ASCII.
pub mod section_identifiers;
/// The sections of a sectioned file.
pub mod sections;
