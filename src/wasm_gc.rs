use std::{
    io::{Cursor, Read, Result as IoResult, Seek, SeekFrom, Write},
    path::PathBuf,
};

/// Remove all custom sections from the WASM file at the specified path.
/// The file is modified in-memory and written to the same file if the section strip is successful.
///
/// ### Parameters:
///
/// * `wasm_file_path`: [`PathBuf`], the path of the WASM contract.
///
pub(crate) fn remove_custom_sections(wasm_file_path: &PathBuf) -> IoResult<()> {
    let read_buf: Vec<u8> = std::fs::read(wasm_file_path)?;
    let file_len = read_buf.len() as u64;

    let mut read: Cursor<Vec<u8>> = Cursor::new(read_buf);

    let mut output: Vec<u8> = Vec::new();
    wasm_strip(file_len, &mut read, &mut output)?;

    std::fs::write(wasm_file_path, &output)
}

/// An enum containing the WASM 1.0 sections.
#[derive(PartialEq, Debug)]
enum WasmSection {
    Custom,
    Type,
    Import,
    Function,
    Table,
    Memory,
    Global,
    Export,
    Start,
    Element,
    Code,
    Data,
    DataCount,
}

/// Parse a section from a [`u8`].
impl TryFrom<u8> for WasmSection {
    type Error = String;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Result::Ok(WasmSection::Custom),
            1 => Result::Ok(WasmSection::Type),
            2 => Result::Ok(WasmSection::Import),
            3 => Result::Ok(WasmSection::Function),
            4 => Result::Ok(WasmSection::Table),
            5 => Result::Ok(WasmSection::Memory),
            6 => Result::Ok(WasmSection::Global),
            7 => Result::Ok(WasmSection::Export),
            8 => Result::Ok(WasmSection::Start),
            9 => Result::Ok(WasmSection::Element),
            10 => Result::Ok(WasmSection::Code),
            11 => Result::Ok(WasmSection::Data),
            12 => Result::Ok(WasmSection::DataCount),
            _ => Result::Err(format!("Invalid section ID: {}", value)),
        }
    }
}

/// The WASM header commonly referred to as the 'magic number'.
const WASM_MAGIC: &[u8] = &[0x00, 0x61, 0x73, 0x6d];
/// The currently supported WASM version
const WASM_VERSION_1: &[u8] = &[0x01, 0x00, 0x00, 0x00];

/// Copy the sections from a WASM, ignoring CUSTOM sections.
/// See the [WASM spec])(https://webassembly.github.io/spec/core/binary/modules.html#sections) for more details.
///
/// The number of sections is not known so the whole file needs to be scanned.
/// The sections have a `[u8]` section type identifier followed by a leb128-encoded length.
/// See [`WasmSection`] above for the supported sections.
///
/// ### Parameters:
///
/// * `file_len`: [`u64`] the length of the input file, this is needed because [`Seek`] `stream_len` is still unstable.
/// * `input`: `&mut` [`R`] the input buffer
/// * `output`: `&mut` [`W`] the output buffer
///
fn wasm_strip<R, W>(file_len: u64, input: &mut R, output: &mut W) -> IoResult<()>
where
    R: Read + Seek,
    W: Write,
{
    let magic = input.read_n(4)?;
    if (&magic as &[u8]) != WASM_MAGIC {
        panic!("Invalid magic header: {:?}", magic);
    }

    let version = input.read_n(4)?;
    if (&version as &[u8]) != WASM_VERSION_1 {
        panic!("Unsupported WASM version: {:?}", version);
    }

    output.write_all(&magic)?;
    output.write_all(&version)?;

    while input.stream_position()? < file_len {
        let raw_section_id = input.read_byte()?;

        let wasm_section_result = WasmSection::try_from(raw_section_id);
        let wasm_section = match wasm_section_result {
            Ok(section) => section,
            Err(e) => panic!("{}", e),
        };

        let (section_len, section_len_data) = input.read_u32_leb128()?;
        // Max section length is based on the blockchain WASM runtime restriction of 31 bits.
        if section_len >= 0x7FFFFFFFu64 {
            panic!("Section {} too long", raw_section_id);
        } else if input.stream_position()? > (0x7FFFFFFFu64 - section_len) {
            panic!(
                "Section {} starting at {} is too long: {}",
                raw_section_id,
                input.stream_position()?,
                section_len
            );
        }

        match wasm_section {
            WasmSection::Custom => {
                input.seek(SeekFrom::Current(section_len as i64))?;
            }
            _ => {
                output.write_all(&[raw_section_id])?;
                output.write_all(&section_len_data)?;

                let section_data = input.read_n(section_len as usize)?;
                output.write_all(&section_data)?;
            }
        }
    }

    Ok(())
}

/// WASM-related binary stream extensions.
trait ReadExtension {
    /// Read a single byte from the stream.
    fn read_byte(&mut self) -> IoResult<u8>;

    /// Read n bytes from the stream as a [`Vec<u8>`].
    fn read_n(&mut self, n: usize) -> IoResult<Vec<u8>>;

    /// Read a 32-bit unsigned leb128 encoded number returning both the number and the raw bytes that encode it.
    fn read_u32_leb128(&mut self) -> IoResult<(u64, Vec<u8>)>;

    // Read a n-bit (n <= 64) unsigned leb128 encoded number.
    fn read_leb128(&mut self, bits: u64) -> IoResult<u64>;
}

impl<R: Read + Seek> ReadExtension for R {
    fn read_byte(&mut self) -> IoResult<u8> {
        let buf = self.read_n(1)?;
        IoResult::Ok(buf[0])
    }

    fn read_n(&mut self, n: usize) -> IoResult<Vec<u8>> {
        let mut buf = vec![0; n];
        buf.resize(n, 0);
        self.read_exact(&mut buf).unwrap();
        Ok(buf)
    }

    fn read_u32_leb128(&mut self) -> IoResult<(u64, Vec<u8>)> {
        let pos_before = self.stream_position().unwrap();
        let value = self.read_leb128(32)?;

        let pos_after = self.stream_position().unwrap();
        let len = pos_after - pos_before;

        self.seek(SeekFrom::Start(pos_before)).unwrap();

        let data = self.read_n(len as usize)?;
        IoResult::Ok((value, data))
    }

    fn read_leb128(&mut self, bits: u64) -> IoResult<u64> {
        let n = self.read_byte()?;
        if n < 128 {
            if bits <= 6 && (n as u64) >= (1u64 << bits) {
                panic!("Integer too large: Too many bits in LEB128 encoded unsigned number.");
            } else {
                IoResult::Ok(n as u64)
            }
        } else if bits <= 7 {
            panic!(
                "Integer representation too long: Too many bytes in LEB128 encoded unsigned number."
            );
        } else {
            let m = self.read_leb128(bits - 7)?;
            IoResult::Ok((m << 7) + ((n as u64) - 128))
        }
    }
}

#[cfg(test)]
mod test {
    use std::io::Cursor;
    use std::path::PathBuf;

    use assert_fs::TempDir;

    use super::{remove_custom_sections, wasm_strip, ReadExtension, WasmSection};

    #[test]
    fn wasm_gc_reference_file() {
        let temp_dir = TempDir::new().unwrap();
        let actual_path = temp_dir.join("contract_with_gc.wasm");

        let input_reference = PathBuf::from("tests/resources/wasm_gc/with_custom_section.wasm");
        std::fs::copy(input_reference, &actual_path).unwrap();
        remove_custom_sections(&actual_path).unwrap();

        let output_reference = PathBuf::from("tests/resources/wasm_gc/no_custom_section.wasm");

        let expected = std::fs::read(output_reference).unwrap();
        let actual = std::fs::read(&actual_path).unwrap();

        assert_eq!(expected, actual);
    }

    #[test]
    #[should_panic(expected = "Invalid magic header: [0, 0, 0, 0]")]
    fn invalid_magic_header() {
        let input: Vec<u8> = vec![0, 0, 0, 0];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);
        let mut output = Vec::new();

        wasm_strip(4, &mut read, &mut output).unwrap();
    }

    #[test]
    #[should_panic(expected = "Unsupported WASM version: [0, 0, 0, 0]")]
    fn invalid_wasm_version() {
        let input: Vec<u8> = vec![0x00, 0x61, 0x73, 0x6d, 0, 0, 0, 0];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);
        let mut output = Vec::new();

        wasm_strip(8, &mut read, &mut output).unwrap();
    }

    #[test]
    #[should_panic(expected = "Invalid section ID: 255")]
    fn unknown_wasm_section() {
        let input: Vec<u8> = vec![0x00, 0x61, 0x73, 0x6d, 1, 0, 0, 0, 0xff];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);
        let mut output = Vec::new();

        wasm_strip(9, &mut read, &mut output).unwrap();
    }

    #[test]
    #[should_panic(expected = "Section 1 too long")]
    fn section_too_long() {
        let input: Vec<u8> = vec![
            0x00, 0x61, 0x73, 0x6d, 1, 0, 0, 0, // magic header and version
            1, // section id
            0xff, 0xff, 0xff, 0xff, 0x0f, // 0xFFFFFFFF as leb128
        ];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);
        let mut output = Vec::new();

        wasm_strip(0xFFFFFFFFu64, &mut read, &mut output).unwrap();
    }

    #[test]
    #[should_panic(expected = "Section 1 starting at 14 is too long: 2147483646")]
    fn section_too_long_2() {
        let input: Vec<u8> = vec![
            0x00, 0x61, 0x73, 0x6d, 1, 0, 0, 0, // magic header and version
            1, // section id
            0xfe, 0xff, 0xff, 0xff, 0x07, // 0x7FFFFFFE as leb128
        ];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);
        let mut output = Vec::new();

        wasm_strip(0x7FFFFFFFu64, &mut read, &mut output).unwrap();
    }

    #[test]
    #[should_panic(
        expected = "Integer too large: Too many bits in LEB128 encoded unsigned number."
    )]
    fn read_leb128_too_large() {
        let input: Vec<u8> = vec![0x7f];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);

        read.read_leb128(6).unwrap();
    }

    #[test]
    #[should_panic(
        expected = "Integer representation too long: Too many bytes in LEB128 encoded unsigned number."
    )]
    fn read_leb128_too_large_2() {
        let input: Vec<u8> = vec![0xff];
        let mut read: Cursor<Vec<u8>> = Cursor::new(input);

        read.read_leb128(7).unwrap();
    }

    #[test]
    fn wasm_section_try_from() {
        assert_eq!(WasmSection::try_from(0).unwrap(), WasmSection::Custom);
        assert_eq!(WasmSection::try_from(1).unwrap(), WasmSection::Type);
        assert_eq!(WasmSection::try_from(2).unwrap(), WasmSection::Import);
        assert_eq!(WasmSection::try_from(3).unwrap(), WasmSection::Function);
        assert_eq!(WasmSection::try_from(4).unwrap(), WasmSection::Table);
        assert_eq!(WasmSection::try_from(5).unwrap(), WasmSection::Memory);
        assert_eq!(WasmSection::try_from(6).unwrap(), WasmSection::Global);
        assert_eq!(WasmSection::try_from(7).unwrap(), WasmSection::Export);
        assert_eq!(WasmSection::try_from(8).unwrap(), WasmSection::Start);
        assert_eq!(WasmSection::try_from(9).unwrap(), WasmSection::Element);
        assert_eq!(WasmSection::try_from(10).unwrap(), WasmSection::Code);
        assert_eq!(WasmSection::try_from(11).unwrap(), WasmSection::Data);
        assert_eq!(WasmSection::try_from(12).unwrap(), WasmSection::DataCount);
    }
}
