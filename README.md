# Cargo `partisia-contract`

Compiles Smart Contracts for the Partisia Blockchain for deployment on-chain.

## Installation

You can either install from the [crate]() on crates.io, or from source.

### From Crates.io

To install run the command

```bash
cargo install cargo-partisia-contract
```

### Install from source

Clone the repository and go to the folder. Run the following command

```bash
cargo install --path .
```

## Usage

Compiles Smart Contracts for the Partisia Blockchain for deployment on-chain.

```text
Usage: cargo partisia-contract <COMMAND>

Commands:
  build          Compile contracts to WASM and generate ABI files.
  init           Initialize the contract. Retrieves dependencies for build.
  new            Create a new smart contract project.
  print-version  Print the client and binder version of the contract.
  path-of-wasm   Print the expected WASM file path based on the context of Cargo.toml
  path-of-abi    Print the expected ABI file path based on the context of Cargo.toml
  set-sdk        Update the sdk used for compiling the contracts.
  cli            Interact with the Partisia blockchain through the partisia-cli.
  help           Print this message or the help of the given subcommand(s)

Options:
  -h, --help
          Print help (see a summary with '-h')

  -V, --version
          Print version

```

### `build`

Compile a smart contract for deployment on Partisia Blockchain.

```text
Usage: cargo partisia-contract build [OPTIONS] [ADDITIONAL_ARGS]...

Arguments:
  [ADDITIONAL_ARGS]...  Additional arguments that will be passed along to cargo build,
                        see cargo build --help for details.

Options:
  -r, --release                        Build artifacts in release mode, with optimizations
  -n, --no-abi                         Skip generating .abi file
  -q, --quiet                          No messages printed to stdout
  -w, --no-wasm-strip                  Do not remove custom sections from the WASM-file (will produce a much larger file).
  -z, --no-zk                          Only compile the public part of the contract. Skips compilation of ZK computation.
      --zkcompiler <ZK_COMPILER_PATH>  Specify path to a zk-compiler JAR to use for compiling the ZK code
      --disable-git-fetch-with-cli     Uses cargo's built-in git library to fetch dependencies instead of the git executable
      --workspace                      Build all packages in the workspace
      --manifest-path <MANIFEST_PATH>  Specify path to the Cargo.toml of the contract or workspace
      --sdk <SDK>                      Override the sdk used for compiling the contracts. Git url and tag/branch/rev can be supplied at the same time or separately.
                                       Example usage:
                                       --sdk "git: https://git@gitlab.com/partisiablockchain/language/contract-sdk.git, tag: 9.1.2"
                                       --sdk "branch: example_branch"
                                       --sdk "rev: 55061d796e5547e3cdf637407d928f95e2e32c59"
      --coverage                       Compile an instrumented binary for the smart contract. This enables generation of coverage files.
  -h, --help                           Print help

```

### `new`

Create a new smart contract project.

```text
Usage: cargo partisia-contract new [OPTIONS] <path> [ADDITIONAL_ARGS]...

Arguments:
  <path>                The contract folder that will be created
  [ADDITIONAL_ARGS]...  Additional arguments that will be passed along to cargo new,
                        see cargo new --help for details.

Options:
  -n, --name <NAME>  Set the name of the contract project created
  -z                 Create a zero knowledge contract
  -h, --help         Print help

```

### `init`

Initialize the contract. Retrieves dependencies for build.

```text
Usage: cargo partisia-contract init [OPTIONS]

Options:
      --workspace                      Init all zk contracts in the workspace
      --manifest-path <MANIFEST_PATH>  Specify path to the Cargo.toml of the contract or workspace
  -h, --help                           Print help

```

### `path-of-abi`

Print the expected ABI file path based on the context of Cargo.toml

```text
Usage: cargo partisia-contract path-of-abi [OPTIONS]

Options:
  -r, --release                        File is in release folder instead of debug
      --manifest-path <MANIFEST_PATH>  Specify path to the Cargo.toml of the contract or workspace
  -h, --help                           Print help

```

### `path-of-wasm`

Print the expected WASM file path based on the context of Cargo.toml

```text
Usage: cargo partisia-contract path-of-wasm [OPTIONS]

Options:
  -r, --release                        File is in release folder instead of debug
      --manifest-path <MANIFEST_PATH>  Specify path to the Cargo.toml of the contract or workspace
  -h, --help                           Print help

```

### `print-version`

Print the client and binder version of the contract.

```text
Usage: cargo partisia-contract print-version [OPTIONS] <WASM contract>

Arguments:
  <WASM contract>  The wasm file to load

Options:
  -b, --bashlike  Print the version as bash variables
  -h, --help      Print help

```

### `set-sdk`

Update the sdk used for compiling the contracts.

```text
Usage: cargo partisia-contract set-sdk [OPTIONS] <sdk>

Arguments:
  <sdk>  The new sdk value. Git url and tag/branch/rev can be supplied at the same time or separately.
         Example usage:
         set-sdk "git: https://git@gitlab.com/partisiablockchain/language/contract-sdk.git, tag: 9.1.2"
         set-sdk "branch: example_branch"
         set-sdk "rev: 55061d796e5547e3cdf637407d928f95e2e32c59"

Options:
      --workspace                      Set the sdk for all packages in the workspace
      --manifest-path <MANIFEST_PATH>  Specify path to the Cargo.toml of the contract or workspace
  -h, --help                           Print help

```

### `cli`

Interact with the Partisia blockchain through the partisia-cli. The specification of the Partisia-CLI
commands can be found [here](https://gitlab.com/partisiablockchain/language/partisia-cli).

```
Interact with the Partisia blockchain through the partisia-cli.

Usage: cargo partisia-contract cli [OPTIONS] [ADDITIONAL_ARGS]...

Arguments:
  [ADDITIONAL_ARGS]...  Additional arguments that will be passed along to partisia-cli.jar

Options:
  -q, --quiet      No messages printed to stdout
  -u, --use <URL>  Url specifying the location to retrieve the partisia-cli JAR from. If not given, a user configuration file in
                   ~/.pbc/config.toml or default values will be used.
                   Uses netrc for authentication.
                   Example usage:
                   --use https://gitlab.com/api/v4/groups/12499775/-/packages/maven/com/partisiablockchain/language/partisia-cli/4.1.0/partisia-cli-4.1.0-jar-with-dependencies.jar
  -h, --help       Print help

```

## How to use

Go into the rust project containing your Cargo.toml and the contract.

An example for a contract written in rust can be found [here](https://partisiablockchain.gitlab.io/documentation/programmers_guide.html).

When you are standing in the directory, run the following command to compile the contract and generate the ABI.

```bash
cargo partisia-contract build --release
```

This will build and write the contract and ABI files in the `target/wasm32-unknown-unknown/debug`.

If you run it with the flag `--release`, then the files will be in `target/wasm32-unknown-unknown/release`.
