//! Constants used for integration tests. These constants should be identical
//! to the constants used in the main package.

/// Environment variable to specify the path to the PBC_FOLDER where
/// downloaded jars, as well as the config.toml is stored.
pub const ENV_VAR_PBC_FOLDER: &str = "PBC_FOLDER";

/// The default location for the pbc folder.
pub const DEFAULT_PBC_FOLDER_NAME: &str = ".pbc";

/// The filename for the config.toml stored in the pbc folder.
pub const CONFIG_TOML_FILE_NAME: &str = "config.toml";

/// Default artifact name for the partisia-cli.
pub const PARTISIA_CLI: &str = "partisia-cli";

pub const ABI_CLI: &str = "abi-cli";

pub const ZK_COMPILER_3_82_0: &str = "https://gitlab.com/api/v4/groups/12499775/-/packages/maven/com/partisiablockchain/language/zkcompiler/3.82.0/zkcompiler-3.82.0-jar-with-dependencies.jar";
pub const ZK_COMPILER_3_63_0: &str = "https://gitlab.com/api/v4/groups/12499775/-/packages/maven/com/partisiablockchain/language/zkcompiler/3.63.0/zkcompiler-3.63.0-jar-with-dependencies.jar";

pub const ZK_COMPILER_4_35_0: &str = "https://gitlab.com/api/v4/groups/12499775/-/packages/maven/com/partisiablockchain/language/zkcompiler/4.35.0/zkcompiler-4.35.0-jar-with-dependencies.jar";

pub const CONTRACTS: &str = "https://gitlab.com/partisiablockchain/language/example-contracts";

/// The zk-compiler version inserted when running cargo partisia-contract new.
pub const NEW_COMMAND_ZK_COMPILER_VERSION: &str = "4.35.0";

/// The path in target where the previous Cargo.toml for each contract package will be saved
/// while running with the new sdk version. This is then read later on to reset the Cargo.toml.
pub const OVERRIDE_SDK_SAVE_TOML_TARGET_PATH: &str = "saved_cargo_toml";
