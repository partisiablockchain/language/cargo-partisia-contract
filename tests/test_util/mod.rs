#![cfg(test)]
// Allow dead code as this module is used separately in different test,
// and all functions aren't used in all tests.
#![allow(dead_code)]

/// Module for abi related test utilities.
pub mod jar_util;

/// Module for the constants used in the package.
pub mod constants;

/// Module for resource test utilities.
pub mod resource_test_util;

/// Module for the BuildReferenceTestRunner.
pub mod build_reference_test_runner;

use assert_cmd::prelude::CommandCargoExt;
use std::env;
use std::path::PathBuf;
use std::process::Command;
use url::Url;

/// Get the resource folder path using the cargo manifest dir env variable.
///
/// ### Returns:
/// The test/resources folder path.
pub fn resource_folder_path() -> PathBuf {
    let root_dir = &env::var("CARGO_MANIFEST_DIR").expect("$CARGO_MANIFEST_DIR");
    let mut source = PathBuf::from(root_dir);
    source.push("tests");
    source.push("resources");
    source
}

/// Get the partisia-contract command using the CARGO_PKG_NAME env variable.
///
/// ### Returns:
/// The cargo partisia-contract command.
pub fn cargo_partisia_command() -> Command {
    let bin_name = env!("CARGO_PKG_NAME");
    let mut cmd = Command::cargo_bin(bin_name).unwrap();
    cmd.arg("partisia-contract");
    // Remove rustflags added by llvm-cov to enable wasm build target
    cmd.env_remove("RUSTFLAGS");
    cmd
}

/// Get the pbc command using the CARGO_PKG_NAME env variable.
///
/// ### Returns:
/// The cargo partisia-contract command.
pub fn cargo_pbc_command() -> Command {
    let bin_name = env!("CARGO_PKG_NAME");
    let mut cmd = Command::cargo_bin(bin_name).unwrap();
    cmd.arg("pbc");
    // Remove rustflags added by llvm-cov to enable wasm build target
    cmd.env_remove("RUSTFLAGS");
    cmd
}

/// Get the file url from a PathBuf, file:/`path`.
///
/// ### Parameters:
///
/// * `path`: [`PathBuf`], the path to the file to get an url for.
///
/// ### Returns:
/// The file url as a String.
pub fn path_to_file_url(path: PathBuf) -> String {
    let path_string = path.into_os_string().into_string().unwrap();
    format!("{}", Url::from_file_path(path_string).unwrap())
}
