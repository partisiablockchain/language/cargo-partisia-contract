use crate::test_util::cargo_partisia_command;
use crate::test_util::constants::{ABI_CLI, ENV_VAR_PBC_FOLDER, PARTISIA_CLI};
use std::fs;
use std::path::{Path, PathBuf};
use std::process::Command;

use toml_edit::DocumentMut;

static_toml::static_toml! {
    static CARGO_TOML = include_toml!("Cargo.toml");
}

/// Get the partisia-contract abi command set up with the given abi folder as an environment variable,
/// and a default url for fetching abi-cli.jar.
///
/// ### Returns:
/// The cargo partisia-contract abi command.
pub fn create_abi_cmd(pbc_folder: &Path) -> Command {
    create_abi_cmd_with_url(get_jar_url(ABI_CLI).as_str(), pbc_folder)
}

/// Get the corresponding filename to the default abi client url.
pub fn default_abi_client_name() -> String {
    PathBuf::from(get_jar_url(ABI_CLI))
        .file_name()
        .and_then(|os_str| os_str.to_str())
        .unwrap()
        .to_string()
}

/// Get the partisia-contract abi command set up with the given abi folder as an environment variable,
/// and using the given url for fetching abi-cli.jar.
///
/// ### Returns:
/// The cargo partisia-contract abi command.
pub fn create_abi_cmd_with_url(url: &str, pbc_folder: &Path) -> Command {
    let mut abi_cmd = cargo_partisia_command();
    abi_cmd.env(ENV_VAR_PBC_FOLDER, pbc_folder);
    abi_cmd.arg("abi");
    abi_cmd.arg("-v");
    abi_cmd.arg("--use");
    abi_cmd.arg(url);
    abi_cmd
}

/// Assert that the config.toml at `pbc_folder/config.toml` is a valid toml and includes
/// \[package.metadata.`artifact`\]
/// url = `url`
pub fn assert_config_toml(pbc_folder: &Path, artifact: &str, url: &str) {
    let new_config = pbc_folder.join("config.toml");
    let content = fs::read_to_string(new_config).unwrap();
    let doc = content.parse::<DocumentMut>().unwrap();
    let item = &doc["package"]["metadata"][artifact]["url"];
    let actual_url = item.as_value().unwrap().as_str().unwrap();
    assert_eq!(actual_url, url);
}

pub fn get_jar_url(jar_identifier: &str) -> String {
    match jar_identifier {
        PARTISIA_CLI => CARGO_TOML.package.metadata.partisia_cli.url.to_string(),
        ABI_CLI => CARGO_TOML.package.metadata.abi_cli.url.to_string(),
        s => {
            panic!("Tried to get default fetchdata for {s}, which does not have a default.");
        }
    }
}
