//! Test that the `cargo pbc` and `cargo partisia-contract` commands are the same underlying
//! binary.
mod test_util;

#[test]
fn cargo_pbc_command_same_as_partisia_contract() {
    let mut pbc = test_util::cargo_pbc_command();
    pbc.arg("account").arg("--version");
    let mut partisia = test_util::cargo_partisia_command();
    partisia.arg("account").arg("--version");

    let pbc_output = pbc.output().unwrap();
    let partisia_output = partisia.output().unwrap();

    assert_eq!(
        String::from_utf8(pbc_output.stdout),
        String::from_utf8(partisia_output.stdout)
    );
}
