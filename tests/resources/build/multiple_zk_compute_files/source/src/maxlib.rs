/// Template zk computation library.
use pbc_zk::*;

fn max(a: Sbi32, b: Sbi32) -> Sbi32 {
    if a < b {
        b
    } else {
        a
    }
}
