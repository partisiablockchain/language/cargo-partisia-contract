/// Template zk computation. Computes the max of the secret variables.
use pbc_zk::*;
use maxlib::max;

pub fn zk_compute() -> Sbi32 {
    let mut res: Sbi32 = Sbi32::from(0);

    // Sum each variable
    for variable_id in secret_variable_ids() {
        res = max(res, load_sbi::<Sbi32>(variable_id));
    }

    res
}
