Building contract average-salary (ABI: true) (release: true)  (zk: true)
 Cargo output:
 Built contract: {{ temp_dir }}/target/wasm32-unknown-unknown/release/average_salary.wasm

 Creating ABI
 wasm_file: {{ temp_dir }}/target/wasm32-unknown-unknown/release/average_salary.wasm
 ABI created: {{ temp_dir }}/target/wasm32-unknown-unknown/release/average_salary.abi

 Using zk-compute-paths: [{{ temp_dir }}/src/zk_compute.rs]
 Compiled zero knowledge computation: {{ temp_dir }}/target/wasm32-unknown-unknown/release/average_salary.zkbc
 Linking wasm and zkbc into zkwa file: {{ temp_dir }}/target/wasm32-unknown-unknown/release/average_salary.zkwa
 Creating .pbc file at: {{ temp_dir }}/target/wasm32-unknown-unknown/release/average_salary.pbc