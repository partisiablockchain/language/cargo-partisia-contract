Building contract token-contract (ABI: true) (release: false)  (zk: false)
 Cargo output:
 Built contract: {{ temp_dir }}/target/wasm32-unknown-unknown/debug/token_contract.wasm

 Creating ABI
 wasm_file: {{ temp_dir }}/target/wasm32-unknown-unknown/debug/token_contract.wasm
 ABI created: {{ temp_dir }}/target/wasm32-unknown-unknown/debug/token_contract.abi

 Creating .pbc file at: {{ temp_dir }}/target/wasm32-unknown-unknown/debug/token_contract.pbc