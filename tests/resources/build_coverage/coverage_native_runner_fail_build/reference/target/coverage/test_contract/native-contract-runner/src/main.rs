//! Native-contract-runner that enables calling the contract as an executable.
//! This is mainly used for coverage testing rust contracts by instrumenting the library through
//! this runner.

use std::env;
use std::io::{Error, ErrorKind};
use std::panic;
use std::vec::Vec;

/// Binder version for the compiled contract.
const BINDER_VERSION: [u8; 3] = [10, 3, 0];

/// Call map, mapping from action names to wrapped functions.
static CALL_MAP: &[(&'static str, extern "C-unwind" fn(*mut u8, usize) -> u64)] = &[
		("action_01", test_contract::__pbc_autogen__set_value_wrapped as extern "C-unwind" fn(*mut u8, usize) -> u64),
		("init", test_contract::__pbc_autogen__initialize_wrapped as extern "C-unwind" fn(*mut u8, usize) -> u64),
];

/// Determines the action to call for the contract.
fn get_action_fn_ptr(function_name: &str) -> extern "C-unwind" fn(*mut u8, usize) -> u64 {
    let wrapped_fn_ptr_opt: Option<extern "C-unwind" fn(*mut u8, usize) -> u64> = CALL_MAP
        .iter()
        .find(|(k, _v)| *k == function_name)
        .map(|(_k, v)| v)
        .copied();

    return match wrapped_fn_ptr_opt {
        Some(wrapped_fn_ptr) => wrapped_fn_ptr,
        None => panic!("Unknown function: {}", function_name),
    }
}

/// Main for contract runner.
///
/// Reads the payload and calls actions in the contract.
/// Uses the first argument as the name of the action to be called. Then reads the first input
/// on stdin as the payload.
fn main() -> Result<(), std::io::Error> {
    let args: Vec<String> = env::args().collect();
    if args.len() <= 1 {
        println!("Usage: {} <action_name> <--debug>", args.get(0).unwrap());
        println!();
        println!("Input: <payload in hex> \\n <avl_payload in hex>");
        println!();
        println!("Send input to stdin followed by \\n.");
        println!();
        return Err(Error::from(ErrorKind::InvalidInput));
    }

    let function_name = args.get(1).unwrap().to_owned();
    let debug = args.len() > 2 && args.get(2) == Some(&"--debug".to_owned());
    if function_name == "--get-binder-version" {
        println!("{}", hex::encode(BINDER_VERSION));
        return Ok(());
    }

    let payload_string = read_line();
    let avl_payload_string = read_line();

    if debug {
        eprintln!(
            "Running function '{}' with payload '{}'\n and avl payload '{}'",
            function_name, payload_string, avl_payload_string
        );
    }

    // Select action function
    let wrapped_fn_ptr = get_action_fn_ptr(&function_name);

    // Read data from input
    let payload: Vec<u8> = hex::decode(payload_string).unwrap();
    let avl_payload: Vec<u8> = hex::decode(avl_payload_string).unwrap();
    pbc_lib::wasm_avl::deserialize_avl_tree(&mut avl_payload.as_slice());

    // Call
    let result: Result<Vec<u8>, _> = panic::catch_unwind(|| {
        let mut payload = payload;
        unsafe {
            let result_u64 = wrapped_fn_ptr(payload.as_mut_ptr(), payload.len());
            let result_ptr = result_u64 as *const u8;

            let length = read_be_u32(&read_n(result_ptr, 4));
            read_n(result_ptr.add(4), length as usize)
        }
    });

    match result {
        Ok(result_data) => println!("{}", hex::encode(result_data)),
        Err(err) => println!("ERROR: {err:?}"),
    };
    Ok(())
}

fn read_line() -> String {
    let mut buffer = String::new();
    let stdin = std::io::stdin();
    stdin.read_line(&mut buffer).unwrap();
    buffer.trim().to_owned()
}

fn read_be_u32(input: &[u8]) -> u32 {
    let mut buf: [u8; 4] = [0u8; 4];
    for i in 0..4 {
        buf[i] = input[i];
    }

    u32::from_be_bytes(buf)
}

unsafe fn read_n(ptr: *const u8, len: usize) -> Vec<u8> {
    if len > 1024 * 1024 * 1024 {
        panic!("Invalid length: {}", len);
    }

    let mut result = Vec::with_capacity(len);
    for i in 0..len {
        result.push(std::ptr::read(ptr.add(i)))
    }
    result
}
