Building contract test_contract (ABI: true) (release: true)  (zk: false)
 Cargo output:
 Built contract: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_contract.wasm

 Creating ABI
 wasm_file: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_contract.wasm
 ABI created: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_contract.abi

 Creating .pbc file at: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_contract.pbc
 Building contract test_zk_contract (ABI: true) (release: true)  (zk: true)
 Cargo output:
 Built contract: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_zk_contract.wasm

 Creating ABI
 wasm_file: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_zk_contract.wasm
 ABI created: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_zk_contract.abi

 Using zk-compute-paths: [{{ temp_dir }}/test-zk-contract/src/zk_compute.rs]
 Compiled zero knowledge computation: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_zk_contract.zkbc
 Linking wasm and zkbc into zkwa file: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_zk_contract.zkwa
 Creating .pbc file at: {{ temp_dir }}/target/wasm32-unknown-unknown/release/test_zk_contract.pbc