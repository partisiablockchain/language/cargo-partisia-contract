//! Run each of the folders with the reference tests.
//! Each test function runs a test for each folder in the provided resource.
mod test_util;

mod reference_test {
    use crate::test_util::build_reference_test_runner::BuildReferenceTestRunner;
    use test_generator::test_resources;

    #[test_resources("tests/resources/set_sdk/*")]
    fn set_sdk(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/path_of/wasm*")]
    fn wasm(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/path_of/abi*")]
    fn abi(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/print_version/*")]
    fn print_version(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/init/*")]
    fn init(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/build/*")]
    fn build(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/workspace/*")]
    fn workspace(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }

    #[test_resources("tests/resources/build_coverage/*")]
    fn coverage(resource: &str) {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    }
}
