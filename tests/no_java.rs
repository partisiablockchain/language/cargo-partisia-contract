//! Test that the program tells a user, if they are missing the required Java installation.
use crate::test_util::build_reference_test_runner::BuildReferenceTestRunner;
use assert_fs::TempDir;
use std::env;
use test_generator::test_resources;

mod test_util;

fn test_without_java<F>(test: F)
where
    F: Fn(),
{
    let java_home = TempDir::new().unwrap();
    let java_home_path = java_home.to_str().unwrap().to_string();
    let path_var = env::var("PATH").unwrap();
    temp_env::with_vars(
        [
            ("JAVA_HOME", Some(java_home_path)),
            ("PATH", Some(path_var)),
        ],
        test,
    );
}

#[test_resources("tests/resources/no_java/*")]
fn no_java(resource: &str) {
    test_without_java(|| {
        let build_test_runner = BuildReferenceTestRunner::from(resource);
        build_test_runner.run_reference_test();
    });
}
